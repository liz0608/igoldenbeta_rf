*** Settings ***
Suite Teardown    Delete All Sessions
Force Tags
Resource          ../../Common/activity_common_resource.txt
Resource          ../../Common/all_common_resourse.txt

*** Test Cases ***
login-0-正常用户名密码登录成功
    [Tags]    smoking
    ${ret}    mobile_post    api.system.user.login    {"phone":"${otherPhone2}","pw":"&{pwmap}[123456]"}    returnlist=error_msg
    Should Be Equal As Strings    ${ret[0]}    success

login-1-港股用户登录成功
    ${ret}    mobile_post    api.system.user.login    {"phone":"${hkPhone}","pw":"&{pwmap}[123456]"}    returnlist=error_msg
    Should Be Equal As Strings    ${ret[0]}    success

login-2-不存在用户名登录失败
    ${ret}    mobile_post    api.system.user.login    {"phone":"19999999999","pw":"&{pwmap}[123456]"}    returnlist=error_msg
    Should Be Equal As Strings    ${ret[0]}    用户或密码错误

login-3-密码错登录失败
    ${ret}    mobile_post    api.system.user.login    {"phone":"${otherPhone2}","pw":"&{pwmap}[1234567]"}    returnlist=error_msg
    Should Be Equal As Strings    ${ret[0]}    用户或密码错误

login-4-用户名空登录失败
    ${ret}    mobile_post    api.system.user.login    {"phone":"","pw":"&{pwmap}[123456]"}    returnlist=error_msg
    Should Be Equal As Strings    ${ret[0]}    用户或密码错误

login-5-黑名单用户登录失败
    [Tags]
    ${ret}    mobile_post    api.system.user.login    {"phone":"${blackPhone}","pw":"&{pwmap}[123456]"}    returnlist=error_msg
    Should Be Equal As Strings    ${ret[0]}    您的帐号存在异常

login-6-用户正常登录，字段校验
    [Tags]    undone
    ${ret}    mobile_post    api.system.user.login    {"phone":"${otherPhone2}","pw":"&{pwmap}[123456]"}    returnlist=error_msg
    Should Be Equal As Strings    ${ret[0]}    success

sms-0-获取注册验证码成功
    [Tags]    smoking
    ${ret}    mobile_post    api.system.user.sms    {"phone":"${smsPhone}","type":"1"}    returnlist=error_msg
    Should Be True    '${ret[0]}' in ['success','今天接收短信数量已达上限，请明天再试']

sms-1-已注册获取注册验证码失败
    [Tags]    smoking
    ${ret}    mobile_post    api.system.user.sms    {"phone":"${otherPhone2}","type":"1"}    returnlist=error_msg
    Should Be True    '${ret[0]}' in ['该手机号已注册','今天接收短信数量已达上限，请明天再试']

sms-2-未注册获取忘记密码验证码失败
    [Documentation]    type=1 注册
    ...
    ...    type=2忘记密码
    ...
    ...
    ...    type=3修改手机号
    ${ret}    mobile_post    api.system.user.sms    {"phone":"${smsPhone}","type":"2"}    returnlist=error_msg
    Should Be Equal    ${ret[0]}    该用户不存在

sms-3-已注册获取忘记密码验证码成功
    [Documentation]    type=1 注册
    ...
    ...    type=2忘记密码
    ...
    ...
    ...    type=3修改手机号
    ${ret}    mobile_post    api.system.user.sms    {"phone":"${otherPhone}","type":"2"}    returnlist=error_msg
    Should Be Equal    ${ret[0]}    success

sms-4-已注册获取修改手机号验证码成功
    [Documentation]    type=1 注册
    ...
    ...    type=2忘记密码
    ...
    ...
    ...    type=3修改手机号
    ${ret}    mobile_post    api.system.user.sms    {"phone":"${otherPhone}","type":"3"}    returnlist=error_msg
    Should Be Equal    ${ret[0]}    success

sms-5-未注册获取修改手机号验证码失败
    [Documentation]    type=1 注册
    ...
    ...    type=2忘记密码
    ...
    ...
    ...    type=3修改手机号
    ${ret}    mobile_post    api.system.user.sms    {"phone":"${smsPhone}","type":"3"}    returnlist=error_msg
    Should Be Equal    ${ret[0]}    因长时间未登录，请重新登录

sms-6-手机号为空获取验证码失败
    [Documentation]    type=1 注册
    ...
    ...    type=2忘记密码
    ...
    ...
    ...    type=3修改手机号
    ${ret}    mobile_post    api.system.user.sms    {"phone":"","type":"1"}    returnlist=error_msg
    Should Be Equal    ${ret[0]}    用户参数非法

reset-1-用户修改密码成功

reset-2-用户sid错误修改密码失败

reset-3-用户修改密码为原密码失败

register-1-正常用户名密码注册成功

validate-1-登录成功，验证session为正常
    [Tags]    smoking
    ${ret}    mobile_post    api.system.user.login    {"phone":"${otherPhone2}","pw":"&{pwmap}[123456]"}    returnlist=error_msg,data.sid,data.userinfo.uid
    Should Be Equal As Strings    ${ret[0]}    success
    Set Suite Variable    ${loginSid}    ${ret[1]}
    Set Suite Variable    ${loginUid}    ${ret[2]}
    ${ret1}    mobile_post    api.system.user.validate.session    sid2=${ret[1]}    returnlist=error_msg
    Should Be Equal As Strings    ${ret1[0]}    success

validate-2-退出登录，验证session为不正常
    [Tags]    smoking
    ${ret}    mobile_post    api.system.user.logout    {'sid':'${loginSid}'}    ${loginSid}    returnlist=error_msg
    Should Be Equal As Strings    ${ret[0]}    success
    ${ret}    mobile_post    api.system.user.validate.session    sid2=${loginSid}    returnlist=error_msg
    Should Be Equal As Strings    ${ret[0]}    因长时间未登录，请重新登录

validate-3-同一账号多次登录，多个session有效
    [Tags]
    ${ret}    mobile_post    api.system.user.login    {"phone":"${otherPhone}","pw":"&{pwmap}[123456]"}    returnlist=error_msg,data.sid
    Should Be Equal As Strings    ${ret[0]}    success
    ${ret1}    mobile_post    api.system.user.login    {"phone":"${otherPhone}","pw":"&{pwmap}[123456]"}    returnlist=error_msg,data.sid
    Should Be Equal As Strings    ${ret1[0]}    success
    ${ret2}    mobile_post    api.system.user.validate.session    sid2=${ret[1]}    returnlist=error_msg
    Should Be Equal As Strings    ${ret2[0]}    success
    ${ret3}    mobile_post    api.system.user.validate.session    sid2=${ret1[1]}    returnlist=error_msg
    Should Be Equal As Strings    ${ret3[0]}    success

validate-4-验证空session不正常
    ${ret1}    mobile_post    api.system.user.validate.session    returnlist=error_msg
    Should Be Equal As Strings    ${ret1[0]}    无效的会话，请重新登录

follow-1-关注一个用户，该用户存在于关注列表

follow-1取消-关注一个用户，用户不存在于关注列表
