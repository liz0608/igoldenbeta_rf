*** Settings ***
Suite Teardown    Delete All Sessions
Resource          ../../Common/activity_common_resource.txt
Resource          ../../Common/all_common_resourse.txt

*** Test Cases ***
1-小金随机问题列表获取成功
    [Tags]    smoking
    ${ret}    mobile_post    api.system.basket.api.index.list    {'limit':'6'}    returnlist=error_msg,data.list
    Should Be Equal As Strings    ${ret[0]}    success
    Length Should Be    ${ret[1]}    6

1-小金随机问题列表不传limit参数
    [Tags]
    ${ret}    mobile_post    api.system.basket.api.index.list    returnlist=error_msg,data.list
    Should Be Equal As Strings    ${ret[0]}    success
    Length Should Be    ${ret[1]}    6

2-A股1分钟K线获取成功
    [Tags]    smoking
    ${resp}    postdata_quot    ${url_quotation}    api.quotation.kline.query    1.0    {"code" : "000001.SZ", "num" : "300","type" : "F1","exType" : "N"}
    log    ${resp.text}
    Should Be Equal As Strings    ${resp.json()['code']}    1000000
    Should Not Be Empty    ${resp.json()['data']['klineList']}

2-A股1分钟K线拉取更多获取成功
    ${time_now}    Get Time
    ${day}    Fetch From Left    ${time_now}    ${SPACE}
    ${time}    Remove String    ${day}    -
    log    ${time}
    ${resp}    postdata_quot    ${url_quotation}    api.quotation.kline.query    1.0    {"code":"002054.SZ","type":"F1","exType":"Q","num":"300","endDate":"${time}1005"}
    log    ${resp.text}
    Should Be Equal As Strings    ${resp.json()['code']}    1000000
    Should Not Be Empty    ${resp.json()['data']['klineList']}

2-港股1分钟K线获取成功
    [Tags]    smoking
    ${resp}    postdata_quot    ${url_quotation}    api.quotation.kline.query    1.0    {"code" : "00700.HK", "num" : "300","type" : "F1","exType" : "N"}
    log    ${resp.text}
    Should Be Equal As Strings    ${resp.json()['code']}    1000000
    Should Not Be Empty    ${resp.json()['data']['klineList']}

2-港股股1分钟K线拉取更多获取成功
    ${time_now}    Get Time
    ${day}    Fetch From Left    ${time_now}    ${SPACE}
    ${time}    Remove String    ${day}    -
    ${resp}    postdata_quot    ${url_quotation}    api.quotation.kline.query    1.0    {"code":"00700.HK","type":"F1","exType":"Q","num":"300","endDate":"${time}1002"}
    log    ${resp.text}
    Should Be Equal As Strings    ${resp.json()['code']}    1000000
    Should Not Be Empty    ${resp.json()['data']['klineList']}

2-A股5分钟K线获取成功
    [Tags]    smoking
    ${resp}    postdata_quot    ${url_quotation}    api.quotation.kline.query    1.0    {"code" : "000001.SZ", "num" : "300","type" : "F5","exType" : "N"}
    log    ${resp.text}
    Should Be Equal As Strings    ${resp.json()['code']}    1000000
    Should Not Be Empty    ${resp.json()['data']['klineList']}

2-港股5分钟K线获取成功
    ${resp}    postdata_quot    ${url_quotation}    api.quotation.kline.query    1.0    {"code" : "00700.HK", "num" : "300","type" : "F5","exType" : "N"}
    log    ${resp.text}
    Should Be Equal As Strings    ${resp.json()['code']}    1000000
    Should Not Be Empty    ${resp.json()['data']['klineList']}

3-A股行情买卖5档获取成功
    [Tags]    smoking
    ${resp}    postdata_quot    ${url_quotation}    api.quotation.timeTrend.queryBuySellList    1.0    {"code":"000001.SZ"}
    Should Be Equal As Strings    ${resp.json()['code']}    1000000
    Length Should Be    ${resp.json()['data']['sellList']}    5
    Length Should Be    ${resp.json()['data']['buyList']}    5

4-搜索增加打新股的返回
    [Tags]    deprecated
    ${ret}    mobile_post    api.system.basket.search    {"keyword":"易鑫集团"}    returnlist=data.resultlist.0    version=8.0
    Should Be Equal As Strings    ${ret[0]['type']}    5
    Should Be Equal As Strings    ${ret[0]['titlename']}    新股

4-搜索港股打新返回3条记录
    ${ret}    mobile_post    api.system.basket.search    {"keyword":"港股打新"}    returnlist=data.resultlist.0    version=8.0
    Should Be Equal As Strings    ${ret[0]['type']}    5
    Should Be Equal As Strings    ${ret[0]['titlename']}    新股
    Comment    Length Should Be    ${ret[0]['list']}    3

4-搜索老版本不返回港股债券
    ${ret}    mobile_post    api.system.basket.search    {"keyword":"04228"}    returnlist=data.resultlist    version=5.0
    Length Should Be    ${ret[0]}    0

4-搜索老版本不返回港股基金
    ${ret}    mobile_post    api.system.basket.search    {"keyword":"82828"}    returnlist=data.resultlist    version=4.0
    Length Should Be    ${ret[0]}    0

4-搜索增加港股基金的返回
    ${ret}    mobile_post    api.system.basket.search    {"keyword":"82828"}    returnlist=data.resultlist.0    version=8.0
    Should Be Equal As Strings    ${ret[0]['type']}    1

4-搜索增加港股债券的返回
    [Tags]
    ${ret}    mobile_post    api.system.basket.search    {"keyword":"04228"}    returnlist=data.resultlist.0    version=8.0
    Should Be Equal As Strings    ${ret[0]['type']}    1

4-港股债券详情获取成功
    [Documentation]    category = 23
    ${ret}    mobile_post    api.system.basket.stockdetail    {"code":"05293.HK","category":"23"}    returnlist=error_msg,data
    Should Be Equal As Strings    ${ret[0]}    success

4-港股基金详情获取成功
    [Documentation]    category = 21
    ${ret}    mobile_post    api.system.basket.stockdetail    {"code":"02828.HK","category":"21"}    returnlist=error_msg,data
    Should Be Equal As Strings    ${ret[0]}    success

4-searchbytype6.0可以搜到港股基金
    ${ret}    mobile_post    api.system.basket.searchbytype    {"keyword":"02828","market":"1"}    returnlist=error_msg,data.result.list    version=6.0
    Should Be Equal As Strings    ${ret[0]}    success
    Length Should Be    ${ret[1]}    1

4-searchbytype5.0不可以搜到港股基金
    ${ret}    mobile_post    api.system.basket.searchbytype    {"keyword":"82828","market":"1"}    returnlist=error_msg,data.result.list    version=5.0
    Should Be Equal As Strings    ${ret[0]}    success
    Should Be Empty    ${ret[1]}

5-组合tab增加新手引导
    ${ret}    mobile_post    api.system.common.new.ad    {'position':'0'}    returnlist=error_msg,data.ad
    Should Be Equal As Strings    ${ret[0]}    success

5-我tab增加新手引导
    ${ret}    mobile_post    api.system.common.new.ad    {'position':'0'}    returnlist=error_msg,data.ad
    Should Be Equal As Strings    ${ret[0]}    success

6-存股宝列表增加返回字段
    [Tags]    smoking
    ${ret}    mobile_post    api.system.cgb.list    {'type':'1'}    returnlist=error_msg,data
    Should Be Equal As Strings    ${ret[0]}    success
    Dictionary Should Contain Key    ${ret[1]['list'][0]}    createTime
    Dictionary Should Contain Key    ${ret[1]['list'][0]}    createRoi
    log    ${ret[1]['list'][0]['createRoi']}
    Should Not Be Equal As Numbers    ${ret[1]['list'][0]['createRoi']}    0.00

7-mini组合列表增加返回字段declareUrl
    [Tags]    smoking
    ${ret}    mobile_post    api.system.mini.list    {'homepage':'0'}    returnlist=error_msg,data
    Should Be Equal As Strings    ${ret[0]}    success
    Dictionary Should Contain Key    ${ret[1]}    declareUrl

7-mini组合首页增加返回字段declareUrl
    ${ret}    mobile_post    api.system.mini.list    {'homepage':'1'}    returnlist=error_msg,data
    Should Be Equal As Strings    ${ret[0]}    success
    Dictionary Should Contain Key    ${ret[1]}    declareUrl

8-邀请好友活动入口新增类型
    [Tags]
    ${ret}    mobile_post    api.system.user.activity    {'type':'3'}    returnlist=error_msg,data
    Should Be Equal As Strings    ${ret[0]}    success
    Should Contain    ${ret[1]['title']}    邀请好友
