*** Settings ***
Suite Setup
Resource          ../../Common/all_common_resourse.txt
Library           ../../Common/Beta_Service.py

*** Test Cases ***
detail4.0正常返回最大回撤字段maxdrawdown
    ${return}    mobile_post    api.system.basket.detail    {"uid":"${uid}","bktid":"141"}    ${sid}    data.detailinfo.maxdrawdown    #小鲜肉
    should be true    0<= abs(${return[0]}) <1

detail4.0增加标签字段
    ${return}    mobile_post    api.system.basket.detail    {"uid":"${uid}","bktid":"141"}    ${sid}    data.detailinfo.labels
    Should Not Be Empty    ${return[0]}

detail4.0增加返回leftstring字段
    ${return}    mobile_post    api.system.basket.detail    {"uid":"${uid}","bktid":"141"}    ${sid}    data.detailinfo
    log    ${return}
    Dictionary Should Contain Key    ${return[0]}    leftstring
