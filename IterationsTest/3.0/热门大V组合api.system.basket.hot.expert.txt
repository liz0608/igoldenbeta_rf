*** Settings ***
Suite Setup
Resource          ../../Common/all_common_resourse.txt
Library           ../../Common/Beta_Service.py

*** Test Cases ***
返回的组合id列表与数据库查询一致
    [Tags]    undone
    log    数据库有垃圾数据，不一致

热门大V首页请求
    [Tags]    smoking
    ${return}    mobile_post    api.system.basket.hot.expert    {"homepage":"1"}    \    data.betaLabelList
    log    ${return}
    ${actlist}    Get Dictionary Keys    ${return[0][0]}
    ${explist}    Evaluate    ['username','name','rateprice','viptag','netValue','basketid','createTime','picurl']
    List Should Contain Sub List    ${actlist}    ${explist}

热门大V组合非首页请求
    ${return}    mobile_post    api.system.basket.hot.expert    {"homepage":"0"}    \    data.betaLabelList
    log    ${return}
    ${actlist}    Get Dictionary Keys    ${return[0][0]}
    ${explist}    Evaluate    ['username','name','rateprice','viptag','netValue','basketid','createTime','picurl']
    List Should Contain Sub List    ${actlist}    ${explist}

返回list增加martket字段
    ${return}    mobile_post    api.system.basket.hot.expert    {"homepage":"0"}    \    data.betaLabelList
    Dictionary Should Contain Key    ${return[0][0]}    market
