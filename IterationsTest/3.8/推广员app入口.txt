*** Settings ***
Resource          ../../Common/activity_common_resource.txt
Resource          ../../Common/all_common_resourse.txt

*** Test Cases ***
最新动态获取成功
    ${ret}    mobile_post    api.system.user.activity    {"uid":"${uid}","type":"1"}    sid2=${sid}    returnlist=code,data
    Should Be Equal As Strings    ${ret[0]}    1000000
    Should not Be Empty    ${ret[1]}

type字段不传，返回最新动态
    ${ret}    mobile_post    api.system.user.activity    {"uid":"${uid}"}    sid2=${sid}    returnlist=code,data
    Should Be Equal As Strings    ${ret[0]}    1000000
    Should not Be Empty    ${ret[1]}

未成为推广员的用户，无入口
    ${ret}    mobile_post    api.system.user.activity    {"uid":"${uid}","type":"2"}    sid2=${sid}    returnlist=code,data
    Should Be Equal As Strings    ${ret[0]}    1000000
    Should Be Empty    ${ret[1]}

用户未登录，接口不成功
    ${ret}    mobile_post    api.system.user.activity    {"type":"2"}    returnlist=code
    Should Be Equal As Strings    ${ret[0]}    2000002

已成为推广员的用户，有推广员入口
    [Tags]    undone
