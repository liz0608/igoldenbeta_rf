*** Settings ***
Suite Setup       登录并设置sid为suite变量    ${pub_phone}    111111
Resource          ../Common/all_common_resourse.txt
Library           OperatingSystem

*** Variables ***
${sheetname}      delete
${filename}       ./data/api.system.basket.xlsx
${pub_phone}      16000000003

*** Test Cases ***
测试用例
    [Tags]    undone
    [Setup]    获取data参数列表    ${filename}    ${sheetname}
    [Template]    测试模板_通用
    1    Y    ${sid}    1.0    101197    1002240564    1000000:success
    2    Y    ${sid}    1.0    ${EMPTY}    1002774213    ${EMPTY}
    3    Y    ${sid}    1.0    -1    1002782777    ${EMPTY}
    4    Y    ${sid}    1.0    1    1002782780    ${EMPTY}
    5    Y    ${sid}    1.0    101197    ${EMPTY}    ${EMPTY}
    6    Y    ${sid}    1.0    101197    9002979854    ${EMPTY}
    7    Y    ${sid}    1.0    101197    639    ${EMPTY}
    9    Y    ${EMPTY}    1.0    101197    1002782782    ${EMPTY}
    [Teardown]    回写测试结果数据    ${filename}    ${sheetname}    ${write_data}
