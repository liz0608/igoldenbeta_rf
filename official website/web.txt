﻿*** Settings ***
Documentation     金贝塔官网的接口自动化测试用例
Force Tags        igoldenbeta
Library           RequestsLibrary
Library           Collections
Library           String
Library           OperatingSystem

*** Test Cases ***
首页请求成功
    [Tags]    igoldenbeta
    Create Session    jbt    https://www.igoldenbeta.com
    ${resp}    Get Request    jbt    /
    Should Be Equal As Strings    ${resp.status_code}    200

首页banner
    create session    igoldenbeta    https://static.igoldenbeta.com
    ${data}    Set Variable    1524454583872
    ${resp}    Get Request    igoldenbeta    /activity/investment-advisor/static/detect-browser.js?_=${data}
    Should Be Equal As Strings    ${resp.status_code}    200

首页banner请求页面数据正确-投顾
    Create Session    jbt    https://hdbeta.igoldenbeta.com
    ${resp}    Get Request    jbt    /activity/investment-advisor/promotion.html?channel=1001
    Should Be Equal As Strings    ${resp.status_code}    200
    Should Contain    ${resp.text}    新财富中国最佳投顾评选

首页banner请求页面数据正确-企业介绍
    [Tags]    igoldenbeta
    Create Session    jbt    https://www.igoldenbeta.com
    ${resp}    Get Request    jbt    /tcontent/post/2531.html
    Should Be Equal As Strings    ${resp.status_code}    200

首页banner请求页面数据正确-金贝塔证券
    [Tags]    igoldenbeta
    Create Session    jbt    https://www.igoldenbeta.com
    ${resp}    Get Request    jbt    /tcontent/post/1865.html
    Should Be Equal As Strings    ${resp.status_code}    200

1英雄榜
    Create Session    jbt    http://www.igoldenbeta.com
    ${header}    Create Dictionary    Content-Type=application/json
    ${data_1}    Set Variable    {"start":"1","limit":"6"}
    ${data}    Create Dictionary    api=api.system.user.webheros    v=1.0    data=${data_1}    imei=123
    ${resp}    post Request    jbt    /api/ajax    headers=${header}    data=${data}
    Should Be Equal As Strings    ${resp.status_code}    200
    log    ${resp.content}
    Should Be Equal As Numbers    ${resp.json()['code']}    1000000

1-0英雄详情static页面
    Create Session    jbt    https://www.igoldenbeta.com
    ${resp}    Get Request    jbt    /tcontent/post/887.html
    Should Be Equal As Strings    ${resp.status_code}    200
    log    ${resp.text}
    Should Contain    ${resp.text}    金贝塔官方制造团队

1-1英雄详情用query string传参
    Create Session    jbt    https://static.igoldenbeta.com
    ${params}    Create Dictionary    v=1525336173624
    ${resp}    Get Request    jbt    /web-native/static/goldbeta.js    params=${params}
    Should Be Equal As Strings    ${resp.status_code}    200

1-3英雄详情
    Create Session    jbt    http://www.igoldenbeta.com
    ${header}    Create Dictionary    Content-Type=application/json
    ${data_1}    Set Variable    {"id":"720350"}    #id是变量
    ${data}    Create Dictionary    api=api.system.user.webheroinfo    v=1.0    data=${data_1}    imei=123
    ${resp}    post Request    jbt    /api/ajax    headers=${header}    data=${data}
    Should Be Equal As Numbers    ${resp.json()['code']}    1000000
    Should Be Equal As Strings    ${resp.json()['data']['info']['id']}    720350

2-0 搜索行业
    Create Session    jbt    http://www.igoldenbeta.com
    ${header}    Create Dictionary    Content-Type=application/json
    ${data_1}    Set Variable    {"keyword":"银行"}    #keyword可以设成变量
    ${data}    Create Dictionary    api=api.system.website.search    v=1.0    data=${data_1}    imei=123
    ${resp}    post Request    jbt    /api/ajax    headers=${header}    data=${data}
    Should Be Equal As Numbers    ${resp.json()['code']}    1000000

3-0 组合详情
    Create Session    jbt    http://www.igoldenbeta.com
    ${header}    Create Dictionary    Content-Type=application/json
    ${data_1}    Set Variable    {"bktid":"1003260299"}    #bktid可以设成变量
    ${data}    Create Dictionary    api=api.system.basket.webDetail    v=3.1    data=${data_1}    imei=123
    ${resp}    post Request    jbt    /api/ajax    headers=${header}    data=${data}
    Should Be Equal As Numbers    ${resp.json()['code']}    1000000

4-0 组合走势
    Create Session    jbt    http://www.igoldenbeta.com
    ${header}    Create Dictionary    Content-Type=application/json
    ${data_1}    Create Dictionary    bktid=1003260299    type=4    #bktid可以设成变量
    ${data}    Create Dictionary    api=api.system.basket.chart    v=3.1    data=${data_1}
    ${resp}    post Request    jbt    /api    headers=${header}    data=${data}
    log    ${resp.json()}
    Should Be Equal As Numbers    ${resp.json()['code']}    1000000

6-2 行情走势分时
    Create Session    jbt    http://www.igoldenbeta.com
    ${header}    Create Dictionary    Content-Type=application/json
    ${data_1}    Create Dictionary    code=399698.sz    startTime=0930    endTime=1500
    ${data}    Create Dictionary    api=api.system.stock.quotation.timetrend.query    v=1.0    data=${data_1}
    ${resp}    post Request    jbt    /api/ajax    headers=${header}    data=${data}
    log    ${resp.content}
    Comment    ${typeresp}    Evaluate    type(${resp.json()})    #判断类型
    Comment    log    ${typeresp}
    Should Be Equal As Numbers    ${resp.json()['code']}    1000000

6-0 指数
    Create Session    jbt    https://www.igoldenbeta.com
    ${resp}    Get Request    jbt    /stock
    Should Be Equal As Strings    ${resp.status_code}    200

7-0 找师傅-官方制造
    Create Session    jbt    https://www.igoldenbeta.com
    ${resp}    Get Request    jbt    /find-master/0
    Should Be Equal As Strings    ${resp.status_code}    200

8-0 找圈子
    Create Session    jbt    https://www.igoldenbeta.com
    ${resp}    Get Request    jbt    /find-circle
    Should Be Equal As Strings    ${resp.status_code}    200

6-1 指数数据校验
    Create Session    jbt    http://www.igoldenbeta.com
    ${header}    Create Dictionary    Content-Type=application/json
    ${data_1}    Create Dictionary    code=399698.sz
    ${data}    Create Dictionary    api=api.system.stock.realtime.index.web    v=1.0    data=${data_1}
    ${resp}    post Request    jbt    /api/ajax    data=${data}    headers=${header}
    ${typeresp}    Evaluate    type(${resp.json()})
    log    ${typeresp}
    Should Be Equal As Numbers    ${resp.json()['code']}    1000000

6-2 行情走势日线
    Create Session    jbt    http://www.igoldenbeta.com
    ${header}    Create Dictionary    Content-Type=application/json
    ${data_1}    Create Dictionary    code=399698.sz    type=D    exType=N    num=60    #type是变量D.W，M
    Comment    ${data}    Set Variable    {api: "api.system.stock.getKline", v: "1.0",data:{code: "399698.sz", type: "D", exType: "N", num: "60"}}
    ${data}    Create Dictionary    api=api.system.stock.getKline    v=1.0    data=${data_1}
    ${resp}    post Request    jbt    /api/ajax    headers=${header}    data=${data}
    Should Be Equal As Strings    ${resp.status_code}    200
    log    ${resp.content}
    Comment    ${typeresp}    Evaluate    type(${resp.json()})    #判断类型
    Comment    log    ${typeresp}
    Should Be Equal As Numbers    ${resp.json()['code']}    1000000
    Should Be Equal As Strings    ${resp.json()['error_msg']}    success

6-2 行情走势周线
    Create Session    jbt    http://www.igoldenbeta.com
    ${header}    Create Dictionary    Content-Type=application/json
    ${data_1}    Create Dictionary    code=399698.sz    type=W    exType=N    num=60
    ${data}    Create Dictionary    api=api.system.stock.getKline    v=1.0    data=${data_1}
    ${resp}    post Request    jbt    /api/ajax    headers=${header}    data=${data}
    Should Be Equal As Strings    ${resp.status_code}    200
    log    ${resp.content}
    Should Be Equal As Numbers    ${resp.json()['code']}    1000000

6-2 行情走势月线
    Create Session    jbt    http://www.igoldenbeta.com
    ${header}    Create Dictionary    Content-Type=application/json
    ${data_1}    Create Dictionary    code=399698.sz    type=M    exType=N    num=60
    ${data}    Create Dictionary    api=api.system.stock.getKline    v=1.0    data=${data_1}
    ${resp}    post Request    jbt    /api/ajax    headers=${header}    data=${data}
    Should Be Equal As Strings    ${resp.status_code}    200
    log    ${resp.content}
    Should Be Equal As Numbers    ${resp.json()['code']}    1000000

7-1精选大V
    Create Session    jbt    https://www.igoldenbeta.com
    ${resp}    Get Request    jbt    /find-master/4
    Should Be Equal As Strings    ${resp.status_code}    200

7-2 精选达人
    Create Session    jbt    https://www.igoldenbeta.com
    ${resp}    Get Request    jbt    /find-master/3
    Should Be Equal As Strings    ${resp.status_code}    200
