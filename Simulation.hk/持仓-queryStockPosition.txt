*** Settings ***
Documentation     https://expmobile.igoldenbeta.com:8445/simhkserver/trade/queryStockPosition?target_uid=1166285&_=1535618566475
Library           RequestsLibrary

*** Variables ***
${url}            https://expmobile.igoldenbeta.com:8445/simhkserver/trade/

*** Test Cases ***
Smoking-主人态查询持仓返回数据正确
    ${headers}    Create Dictionary    Content-Type=application/x-www-form-urlencoded;charset=utf-8
    Create Session    SHK    ${url}    headers=${headers}
    ${params}    Create Dictionary    target_uid=1166285
    ${resp}    Get Request    SHK    /queryStockPosition    params=${params}
    Should Be Equal As Strings    ${resp.json()['code']}    1000000
    log    ${resp.json()}

未登录，返回数据正确

入参错误-target_uid未报名
