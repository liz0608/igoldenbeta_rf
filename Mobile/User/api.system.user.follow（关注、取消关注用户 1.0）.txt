*** Settings ***
Suite Setup       登录并设置sid为suite变量    ${loginPhone}    111111
Suite Teardown    Delete All Sessions
Force Tags
Resource          ../../Common/all_common_resourse.txt

*** Test Cases ***
smoking-关注一个用户，该用户存在于关注列表
    ${ret}    mobile_post    api.system.user.follow    {"followid":"1","uid":"${uid}","type":"0"}    sid2=${sid}    returnlist=error_msg    version=1.0
    Should Be Equal As Strings    ${ret[0]}    success

smoking-取消-关注一个用户，用户从关注列表移除
    ${ret}    mobile_post    api.system.user.follow    {"followid":"1","uid":"${uid}","type":"1"}    sid2=${sid}    returnlist=error_msg    version=1.0
    Should Be Equal As Strings    ${ret[0]}    success
