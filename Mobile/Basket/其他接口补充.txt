*** Settings ***
Suite Teardown    Delete All Sessions
Resource          ../../Common/activity_common_resource.txt
Resource          ../../Common/all_common_resourse.txt

*** Test Cases ***
redpoint-已登录，请求成功
    [Tags]    smoking    testemail
    ${return}    mobile_post    api.system.notify.redpoint    {"uid":"${uid}"}    returnlist=code    sid2=${sid}    #api.system.notify.redpoint
    Should Be Equal As Strings    ${return[0]}    1000000

redpoint-未登录，请求失败
    ${return}    mobile_post    api.system.notify.redpoint    {"uid":"${uid}"}    returnlist=code
    Should Be Equal As Strings    ${return[0]}    1003001

评论推送成功
    [Documentation]    手机未登录的话，评论无法推送
    [Tags]
    ${list}    Evaluate    [{"ext":"","length":6,"type":1,"refId":"101212","start":3}]
    ${return}    postdata    ${url_http}    api.system.feed.post    1.0    {"content":"楼梯 @工行小公举","uid":"${uid}","type":"1","refList":${list}}    ${sid}

手机直接推送成功
    [Tags]    undone

股票分时线获取
    [Tags]    smoking
    ${ret}    mobile_post    api.system.stock.favoritelist    {"uid":"${uid}","targetid":"${uid}","orderType":"0","orderWay":"0"}    ${sid}    data.list
    ${radnum}    Evaluate    random.randint(0,len($ret[0])-1)    random
    ${stockcode}    Set Variable    ${ret[0][${radnum}]['code']}
    ${resp}    postdata_quot    ${url_quotation}    api.quotation.timeTrend.query    1.0    {"endTime" : "1500","startTime" : "0930","code" : "${stockcode}"}
    log    ${resp.text}
    Should Be Equal As Strings    ${resp.json()['code']}    1000000
    Comment    ${timenow}    get time    hour min
    Comment    ${beforeOpen}    Create List    09    30
    Comment    Run Keyword If    ${timenow} > ${beforeOpen}    Should Not Be Empty    ${resp.json()['data']['timeTrends']}

股票tag返回
    ${ret}    mobile_post    api.system.stock.favoritelist    {"uid":"${uid}","targetid":"${uid}","orderType":"0","orderWay":"0"}    ${sid}    data.list
    ${radnum}    Evaluate    random.randint(0,len($ret[0])-1)    random
    ${stockcode}    Set Variable    ${ret[0][${radnum}]['code']}
    ${ret1}    mobile_post    api.system.tag.getStockTags    {'stockCode':'${stockcode}'}    ${sid}    error_msg,data.list
    Should Be Equal As Strings    ${ret1[0]}    success

股票listvip返回成功
    ${ret}    mobile_post    api.system.stock.favoritelist    {"uid":"${uid}","targetid":"${uid}","orderType":"0","orderWay":"0"}    ${sid}    data.list
    ${radnum}    Evaluate    random.randint(0,len($ret[0])-1)    random
    ${stockcode}    Set Variable    ${ret[0][${radnum}]['code']}
    ${ret1}    mobile_post    api.system.stock.listvip    {'code':'${stockcode}'}    ${sid}    error_msg,data.list
    Should Be Equal As Strings    ${ret1[0]}    success

新增自选股票，成功
    [Tags]    smoking
    ${ret}    mobile_post    api.system.basket.searchbytype    {"limit":"200","uid":"${uid}","keyword":"00","market":"0"}    returnlist=data.result.list
    ${radnum}    Evaluate    random.randint(0,len($ret[0])-1)    random
    Set suite Variable    ${stockcode}    ${ret[0][${radnum}]['code']}
    ${ret1}    mobile_post    api.system.stock.favorite    {"code":"${stockcode}","uid":"${uid}","type":"0"}    ${sid}    code
    Should Be Equal As Strings    ${ret1[0]}    1000000
    ${ret2}    mobile_post    api.system.stock.favoritelist    {"uid":"${uid}","targetid":"${uid}","limit":"60"}    ${sid}    data.list
    ${codes}    Get Dict List    ${ret2[0]}    code
    List Should Contain Value    ${codes}    ${stockcode}    自选后，自选列表存在该股票

新增港股自选股票，失败
    [Tags]
    ${ret}    mobile_post    api.system.basket.searchbytype    {"limit":"200","uid":"${uid}","keyword":"00","market":"1"}    returnlist=data.result.list
    ${radnum}    Evaluate    random.randint(0,len($ret[0])-1)    random
    Set suite Variable    ${stockcode}    ${ret[0][${radnum}]['code']}
    ${ret1}    mobile_post    api.system.stock.favorite    {"code":"${stockcode}","uid":"${uid}","type":"0"}    ${sid}    error_msg
    Should Be Equal As Strings    ${ret1[0]}    您的港股自选股票已到达20只上限，请先删除其他自选股票后再添加

取消自选股票，成功
    [Tags]    smoking
    ${ret3}    mobile_post    api.system.stock.favorite    {"code":"${stockcode}","uid":"${uid}","type":"1"}    ${sid}    code
    Should Be Equal As Strings    ${ret3[0]}    1000000
    ${ret2}    mobile_post    api.system.stock.favoritelist    {"uid":"${uid}","targetid":"${uid}","limit":"60"}    ${sid}    data.list
    ${codes}    Get Dict List    ${ret2[0]}    code
    List Should not Contain Value    ${codes}    ${stockcode}    取消自选后，自选列表不存在该股票

文件上传接口（评论时）
    [Tags]    undone

用户行为文件上传接口
    [Tags]    undone

基金股票详情获取成功
    [Tags]    undone

基金股票的分时及5日获取成功
    [Tags]    undone

债券股票详情获取成功
    [Tags]    undone
