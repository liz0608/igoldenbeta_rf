*** Settings ***
Suite Teardown    Delete All Sessions
Resource          ../../Common/activity_common_resource.txt
Resource          ../../Common/all_common_resourse.txt

*** Test Cases ***
获取某一组合调仓列表成功
    [Tags]    smoking
    ${return}    mobile_post    api.system.basket.statuslist    {"uid":"${uid}"}    ${sid}    data.grouplist
    ${groupNames}    Get Dict List    ${return[0]}    groupname
    List Should Contain Value    ${groupNames}    未发布
    ${groupIndex}    Get Index From List    ${groupNames}    未发布
    Comment    log    ${return[0][${groupIndex}]}
    ${radnum}    Evaluate    random.randint(0,len($return[0][${groupIndex}]['basketlist'])-1)    random
    Set suite Variable    ${bktid}    ${return[0][${groupIndex}]['basketlist'][${radnum}]['basketid']}
    ${ret1}    mobile_post    api.system.basket.stockadjust    {'basketid':'${bktid}','uid':'${uid}'}    ${sid}    error_msg
    Should Be Equal As Strings    ${ret1[0]}    success

未登录获取某一组合调仓列表失败
    [Documentation]    没有失败，原实现就有问题
    [Tags]
    ${ret1}    mobile_post    api.system.basket.stockadjust    {'basketid':'${bktid}','uid':'${uid}'}    \    error_msg
    Should Be Equal As Strings    ${ret1[0]}    success

对已有组合进行调仓，调仓成功
    [Tags]    undone
    ${ret}    mobile_post    api.system.basket.stockadjust    {'basketid':'${bktid}','uid':'${uid}'}    ${sid}    error_msg,data.adjustlist
    Should Be Equal As Strings    ${ret1[0]}    success
    ${stocks}    Get Dict List    ${ret[1]}    code
    ${weights}    Get Dict List    ${ret[1]}    weight

获取调仓记录，成功
    [Tags]    undone
    ${ret}    mobile_post    api.system.basket.stockadjust    {'basketid':'${bktid}','uid':'${uid}'}    ${sid}    error_msg,data.adjustlist
    Should Be Equal As Strings    ${ret1[0]}    success
    ${stocks}    Get Dict List    ${ret[1]}    code
    ${weights}    Get Dict List    ${ret[1]}    weight

对正在进行的调仓，撤销调仓
    [Documentation]    撤消成功，刷新后状态为2已撤单
    [Tags]    undone
    ${ret}    mobile_post    api.system.basket.stockadjust    {'basketid':'${bktid}','uid':'${uid}'}    ${sid}    error_msg,data.adjustlist
    Should Be Equal As Strings    ${ret1[0]}    success
    ${stocks}    Get Dict List    ${ret[1]}    code
    ${weights}    Get Dict List    ${ret[1]}    weight
