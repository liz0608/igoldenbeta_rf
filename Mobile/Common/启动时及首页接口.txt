*** Settings ***
Suite Teardown    Delete All Sessions
Resource          ../../Common/activity_common_resource.txt
Resource          ../../Common/all_common_resourse.txt

*** Test Cases ***
app.patch成功
    [Tags]    smoking
    ${resp}    mobile_post    api.system.common.app.patch    {"appversion":"32"}    returnlist=code    #现只有安卓可补丁
    Should Be Equal As Strings    ${resp[0]}    1000000

beta.const成功
    [Tags]    smoking
    ${resp}    mobile_post    api.system.common.beta.const    {}    returnlist=code
    Should Be Equal As Strings    ${resp[0]}    1000000

common.update成功
    [Tags]    smoking
    ${resp}    mobile_post    api.system.common.update    {"version":"34","platform":"1"}    returnlist=code
    Should Be Equal As Strings    ${resp[0]}    1000000

首页顶部banner返回成功
    [Tags]    smoking
    ${resp}    mobile_post    api.system.basket.home.operation    {"type":"1"}    ${sid}    code,data.list.0
    Should Be Equal As Strings    ${resp[0]}    1000000
    Should Not Be Empty    ${resp[1]}

指数接口，返回3个指数信息
    [Tags]    smoking
    ${resp}    mobile_post    api.system.basket.realtime.index    {}    returnlist=data.realList
    ${names}    Get Dict List    ${resp[0]}    shortName
    ${expList}    Evaluate    ['上证指数','深证成指','创业板指']
    List Should Contain Sub List    ${names}    ${expList}

英雄榜接口，响应正常
    [Tags]    smoking
    ${resp}    mobile_post    api.system.user.listhero    {"type":"1"}    returnlist=code
    Should Be Equal As Strings    ${resp[0]}    1000000

首页最新调仓记录
    [Tags]    smoking
    ${resp}    mobile_post    api.system.basket.adjustlist    {"uid":"${uid}"}    returnlist=code
    Should Be Equal As Strings    ${resp[0]}    1000000

英雄榜点进去的接口
    [Tags]    smoking
    ${resp}    mobile_post    api.system.user.list    { \ \ "limit" : "20", \ \ "uid" : "101213", \ \ "auth" : "1", \ \ "start" : "-1" }    returnlist=code,data.userlist
    Should Be Equal As Strings    ${resp[0]}    1000000
    ${uids}    Get Dict List    ${resp[1]}    uid
    Set Suite Variable    ${uids}

英雄榜点进去的接口，下拉正常
    [Tags]
    ${resp}    mobile_post    api.system.user.list    { "limit" : "20", "uid" : "101213","auth" : "1", "start" : "21" }    returnlist=data.userlist
    ${uids1}    Get Dict List    ${resp[0]}    uid
    ${unionList}    Evaluate    list(set(${uids}).intersection(set(${uids1})))    #下拉后从第21个开始，无交集
    Should Be Empty    ${unionList}

add.token接口
    [Tags]    undone

push.register接口
    [Tags]    undone
