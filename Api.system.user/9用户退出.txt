*** Settings ***
Suite Setup       登录并设置sid为suite变量    ${pub_phone}    123456
Resource          ../Common/all_common_resourse.txt
Library           OperatingSystem

*** Variables ***
${sheetname}      logout
${pub_phone}      13100001112
${filename}       ./data/api.system.user.xlsx

*** Test Cases ***    No            run           sid             v               data.uid         data.outertoken                                                            data.innertoken                           ExpectedRst
logout测试模板            [Setup]       获取data参数列表    ${filename}     ${sheetname}
                      [Template]    测试模板_通用
                      1             Y             ${EMPTY}        1.0             101064           6c3451a0 face2393 9b35aac5 cd8d53e6 ae69c8bf 6878cd8d 28f744d9 e611553b    0-998945de-e0aa-a8ba-7cb0-603ffc2e8120    1000000:success
                      2             Y             ${EMPTY}        1.0             ${EMPTY}         6c3451a0 face2393 9b35aac5 cd8d53e6 ae69c8bf 6878cd8d 28f744d9 e611553b    0-998945de-e0aa-a8ba-7cb0-603ffc2e8120    1000000:success
                      3             Y             ${EMPTY}        1.0             6666666          6c3451a0 face2393 9b35aac5 cd8d53e6 ae69c8bf 6878cd8d 28f744d9 e611553b    0-998945de-e0aa-a8ba-7cb0-603ffc2e8120    1000000:success
                      4             Y             ${EMPTY}        1.0             101064           ${EMPTY}                                                                   0-998945de-e0aa-a8ba-7cb0-603ffc2e8120    1000000:success
                      5             Y             ${EMPTY}        1.0             101064           6c3451a0 face2393 9b35aac5 cd8d53e6 jghj                                   0-998945de-e0aa-a8ba-7cb0-603ffc2e8120    1000000:success
                      6             Y             ${EMPTY}        1.0             101064           6c3451a0 face2393 9b35aac5 cd8d53e6 ae69c8bf 6878cd8d 28f744d9 e611553b    ${EMPTY}                                  1000000:success
                      7             Y             ${EMPTY}        1.0             101064           6c3451a0 face2393 9b35aac5 cd8d53e6 ae69c8bf 6878cd8d 28f744d9 e611553b    0-998945de-e0aa-a8ba-7cb                  1000000:success
                      8             Y             ${EMPTY}        ${EMPTY}        101064           6c3451a0 face2393 9b35aac5 cd8d53e6 ae69c8bf 6878cd8d 28f744d9 e611553b    0-998945de-e0aa-a8ba-7cb0-603ffc2e8120    2000003:服务器遇到问题，请稍后重试
                      9             Y             ${EMPTY}        111.0           101064           6c3451a0 face2393 9b35aac5 cd8d53e6 ae69c8bf 6878cd8d 28f744d9 e611553b    0-998945de-e0aa-a8ba-7cb0-603ffc2e8120    1000001:服务器遇到问题，请稍后重试
                      10            Y             ${EMPTY}        1.0             101064           6c3451a0 face2393 9b35aac5 cd8d53e6 ae69c8bf 6878cd8d 28f744d9 e611553b    0-998945de-e0aa-a8ba-7cb0-603ffc2e8120    1000000:success
                      11            Y             161465465777    1.0             101064           6c3451a0 face2393 9b35aac5 cd8d53e6 ae69c8bf 6878cd8d 28f744d9 e611553b    0-998945de-e0aa-a8ba-7cb0-603ffc2e8120    1000000:success
                      [Teardown]    回写测试结果数据      ${filename}     ${sheetname}    ${write_data}

*** Keywords ***
