#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import hashlib
import random

class md5sum(object):
    def get_sign(self,api,v,data,imei="",imsi="",t=""):
        '''
        Examples:
        ${sign}  | Get Sign  | ${api} | 2.0 | ${inner_data}  | 357070058113616  | ${EMPTY} |  ${t_now}

        '''

        #key string
        key="57f27786442bc2c946780746c8baa0c6"

        # const md5sum
        const = hashlib.md5()
        const.update("29129215")
        const_md5 = const.hexdigest()

        #data md5sum
        data_md5 = hashlib.md5()
        data_md5.update(data)
        data_md5 = data_md5.hexdigest()


        signbefore =  key + "&" + const_md5 + "&" + api + "&" + v + "&" + imei + "&" + imsi +  "&" +\
                      data_md5 + "&" + t

        #sign md5sum
        sign = hashlib.md5()
        sign.update(signbefore)
        sign = sign.hexdigest()

        return sign

    def get_sign2(self,api,data):
        #key
        keys="f447da1467a949f0aac6efabf19c6438"
        str=''
        for d in data:
            if str=='':
                str=d+"="+data[d]
            else:
                str = str +"&"+ d+"="+data[d]
        signbefore =  api + "&" + str + "&" +   "kdata=" + keys
        #print "signbefore=" + signbefore

        #sign md5sum
        sign = hashlib.md5()
        sign.update(signbefore)
        sign = sign.hexdigest()
        #print "signafter=" + sign

        data['key']=sign
        #print data

        return data

    def get_sign3(self):
        #key
        token='beb738c197ba78a60b8b34d125fc4740'
        times=random.randint(1501346962685,1597346962685)
        str1 =  "token="+token + "&time=" + str(times) + "&jsfund"
        #print "signbefore=" + signbefore

        #sign md5sum
        sign = hashlib.md5()
        sign.update(str1)
        sign = sign.hexdigest()

        #print "signafter=" + sign
        url= "http://10.142.31.140:8180/advisor-rank/userVCR?token="+token + "&time=" + str(times) + "&sign="+sign.upper()
        print url
        return url