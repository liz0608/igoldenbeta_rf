#!/usr/bin/env python
# -*- coding:utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from excel import *
from  openpyxl import *
from openpyxl.styles import *
from openpyxl.reader.excel import load_workbook
import comVarsHK
import json
# from Crypto.Cipher import PKCS1_OAEP
# from Crypto.Cipher import PKCS1_v1_5
# from Crypto.PublicKey import RSA
import base64
# from M2Crypto import BIO, RSA



def get_data(parameters=[], datavalues=[]):
    '''
        build the data through the tow input list
        ${data} | get data | ${paras} | ${values}
    '''

    data = {}
    i = 0
    for para in parameters:
        if 'data' in para:
            data[para[5:]] = datavalues[i]
        i = i + 1

    return data
    #if len(data) == 0:
    #    return '{}'
    #else:
    #    strdic = '{"'
    #   strdic = strdic + '","'.join(['":"'.join((k, str(data[k]))) for k in sorted(data, key=data.get, reverse=True)])
    #    strdic = strdic + '"}'
    #    return strdic


def get_parameter(file, sheetName):
    '''
        get the keylist which contains 'data'
        ${data} | get data | ${paras} | ${values}
    '''
    parameter = []
    colsNum = excel.excelColCount(file,sheetName)

    for j in range(colsNum):
        data = excel.getCellData(file,sheetName,1,j)
        if 'data.' in data:
            parameter.append(data)
    return parameter


def change_data(dictIn):
    '''
        change data to a standard form, for example
        ${databefore} | set variable | {"phone":"null","type":"1"}
        ${dataafter} | change data | ${databefore}
        log  | ${dataafter}
        the result is: {"type":"1"}
    '''

    #dictIn = eval(stringIn)
    #print dictIn
    print dictIn
    for key ,value in dictIn.items():
        if value == 'null':
            dictIn.pop(key)

    if len(dictIn) == 0:
        return '{}'
    else:
        strdic = '{"'
        strdic = strdic + '","'.join(['":"'.join((k, str(dictIn[k]))) for k in sorted(dictIn, key=dictIn.get, reverse=True)])
        strdic = strdic + '"}'
        strdic=strdic.replace('"[','[').replace(']"',']')
        return strdic



def mobile_paras(apiName,dataStr='{}'):
    datadict = comVarsHK.MOBILE_DEFAULT_VALUE[apiName]['data']
    apiVersion = comVarsHK.MOBILE_DEFAULT_VALUE[apiName]['v']
    url = comVarsHK.MOBILE_DEFAULT_VALUE[apiName].has_key('protocol')
    if dataStr == '':
        dataStr = '{}'
    dataNew = eval(dataStr)
    datadictCopy = datadict.copy()
    datadictCopy.update(dataNew)
    if apiName=='api.system.basket.create' or apiName=='api.system.basket.update':
        finalData = change_data(datadictCopy)
    else:
        finalData = json.dumps(datadictCopy,ensure_ascii=False)
    return [url,apiName,apiVersion,finalData]


def update_dictionary(dictOrgin, dataStr='{}',otherdict={}):
    if dataStr == '':
        dataStr = '{}'
    dataNew = eval(dataStr)
    dictCopy  = dictOrgin.copy()
    dictCopy.update(dataNew)
    dictCopy.update(otherdict)

    return dictCopy

def get_dict_list(listDict, params, changeToNum=0):
    '''
    Example:
    Suppose alist =[{'code':'1'},{'code':'2'},{'code':'3'}]
    ${list}  getDictList  alist  code
    ${list} will be ['1','2','3']
    '''
    if changeToNum == '1':
        return  [float(listDict[i][params]) for i in range(len(listDict))]
    else:
        return  [listDict[i][params] for i in range(len(listDict))]


def get_RSA_encrypt(publickeyStr, beforeEn="123abc"):
    # start = '-----BEGIN PUBLIC KEY-----\n'
    # end = '\n-----END PUBLIC KEY-----'
    # preEn = start+publickeyStr+end
    # bio = BIO.MemoryBuffer(preEn)
    # rsa = RSA.load_pub_key_bio(bio)
    # encrypted = rsa.public_encrypt(beforeEn, RSA.no_padding)
    # # rsa_pri = M2Crypto.RSA.load_key('E:/public.pem')
    # # encrypted = rsa_pri.public_encrypt(bytes(beforeEn), M2Crypto.RSA.pkcs1_padding)
    # # public_key = rsa.PublicKey.load_pkcs1_openssl_pem(preEn)
    # # encrypt_msg = base64.b64encode(rsa.encrypt(beforeEn, public_key))
    # #return encrypt_msg
    # # pubkey = RSA.importKey(base64.b64decode(publickeyStr.replace('\n','')))
    # # cipher = PKCS1_OAEP.new(pubkey)
    # # encrypted =cipher.encrypt(bytes(beforeEn))
    # return str(base64.b64encode(encrypted))
    pass


if __name__=='__main__':
    str ="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCazmFM2OEAwKjZJeVnC+rl6dTF+xW1HmGEv79W\nrgoh77FEoJVQGfNVtA/B+g5B1dF0hoPdn4o55I8eadKn0g1VvB/zllc5zeMVsThDN4r/DKSQeffP\nVkHbeydSNPlTGknZVRv1FSsPCTuonwP1hWjDxWbNlsl0i5SLZ+4uIsWaNQIDAQAB"

    en=get_RSA_encrypt(str,'123abc')
    print(en)