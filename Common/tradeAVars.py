#!/usr/bin/env python
# -*- coding: utf-8 -*-

#10.31.90.109 9999 10.31.90.102 1090 121.40.156.222 9090
# SocketIp='10.31.90.102'
# IntSocket=1090
# Version='V_1_0'
# A stock

account_map ={
    "13100001112":{"truid":"101213","fundid":"1000016","trdpwd":"1234abcd","secuid":"1000016"},#HK 1
    "13100001115":{"truid":"101215","fundid":"1000015","trdpwd":"1234abcd","secuid":"1000015"}
    }

def get_variables(arg):
    if arg == '06':
        return TRADE_DEFAULT_DIC('13100001112')
    elif arg == '02':
        return TRADE_DEFAULT_DIC('13100001115')


#trade
def TRADE_DEFAULT_DIC(account):
    fundid = account_map[account]['fundid']
    trdpwd = account_map[account]['trdpwd']
    secuid = account_map[account]['secuid']
    truid  = account_map[account]['truid']

    TRADE_DEFAULT_DIC1={
                 "customerService.getVerificationCode":{"dynamicMap": {"imei": account, "debug" : "true"}},
                 "customerService.isShowAgreement":{},
                 "customerService.login":{"dynamicMap": {"inputtype": "C","inputid": fundid,"trdpwd": trdpwd,"verifyCode": "","imei": account}},
                 "customerService.logout":{"dynamicMap": {"sessionId": "sessionId?", "imei": account}},
                 "queryService.queryFund":{"dynamicMap": {"sessionId": "","fundid": fundid,"moneytype": "0"}},
                 "queryService.queryStock":{"dynamicMap": {"sessionId": "sessionId?","fundid": fundid,"qryflag": "1","count": "15","stkcode": "stkcode?","market": "market?","poststr": ""}},
                 "queryService.queryOrders":{"dynamicMap": {"sessionId": "sessionId?","fundid": fundid,"qryflag": "1","count": "1000","poststr": ""}},
                 "queryService.queryDone":{"dynamicMap": {"sessionId": "sessionId?","fundid": fundid,"qryflag": "1","count": "1000","poststr": ""}},
                 "queryService.getMaxTranseCount":{"dynamicMap": {"sessionId": "sessionId?","market": "0","secuid": secuid,"fundid": fundid,"stkcode": "000010","bsflag": "0B","price": "6.86"}},
                 "queryService.queryAvailableFund":{"dynamicMap": {"sessionId": "sessionId?","fundid": fundid,"moneytype": "0"}},
                 "queryService.queryMatch":{"dynamicMap": { "sessionId" : "sessionId?", "pattern" : "pattern?" }},
                 "queryService.queryMarket":{"dynamicMap": { "sessionId" : "sessionId?", "code" : "code?", "market": "market?"}},
                 "queryService.queryMatchExact":{"dynamicMap": { "sessionId" : "sessionId?", "stkcode" : "code?","market": "0" }},
                 "queryService.queryStockCombination":{"dynamicMap": {"sessionId": "sessionId?","detail": ""}},
                 "tradeService.confirmTrans":{"dynamicMap": {"sessionId": "sessionId?","fundid": fundid,"secuid": secuid,"stkcode": "000011","market": "0","bsflag": "0B","price": "11","qty": "100","ordergroup": "0","chkriskflag":"0"}},
                 "tradeService.cancelTrans":{"dynamicMap": {"sessionId": "sessionId?","fundid": fundid,"ordersno": ""}},
                 "tradeService.changeTrans":{"dynamicMap": {"sessionId": "sessionId?","fundid": fundid,"ordersno": "","price": "1.48","qty": "2000"}},
                 "tradeService.confirmCombination":{"dynamicMap": {"sessionId": "sessionId?","fundid": fundid,"detail":[
                     {"secuid":secuid,"stkcode":"600081","market":"1","bsflag":"0B","price":"15.56","qty":"100","ordergroup":"0","chkriskflag":"0"},
                     {"secuid":secuid,"stkcode":"600094","market":"1","bsflag":"0B","price":"10.56","qty":"200","ordergroup":"0","chkriskflag":"0"}]}},
                 "mobileTransService.verfyLogin": {"dynamicMap": {"imei": account,"uid":truid}},
                 "mobileTransService.getSecurities":{"dynamicMap": {"imei": account}},
                 "customerService.queryUserInfo":{"dynamicMap": {"sessionId": "sessionId?", "custid": fundid}},
                 "customerService.modifyUserInfo":{"dynamicMap": {"sessionId": "sessionId?", "custid": fundid,"email": "","telephone": "3420"}},
                 "customerService.forgotPassword":{"dynamicMap": {"sessionId": "sessionId?", "custid": fundid,"name": "name?", "idcardno": "idcardno?","email": "","telephone": "3420"}},
                 "bankStockService.transferBankToSecurity":{"dynamicMap": {"sessionId":"sessionId?","fundid":fundid,"name": "name?", "bank": "bank?","bankcardno":"bankcardno?","tranamt":"3420","tranflowno":"tranflowno?","cur":"0"}},
                 "bankStockService.transferSecurityToBank":{"dynamicMap": {"sessionId": "sessionId?", "fundid": fundid,"tranamt":"3420","cur":"0"}},
                 "bankStockService.queryBankSecuTrans":{"dynamicMap": {"sessionId": "sessionId?", "fundid": fundid}},
             "bankStockService.transferAccount":{"dynamicMap": {"sessionId": "","fundid" : fundid, "moneytype": "0","fundpwd": "111111","bankcode": "1002","bankpwd": "111111","banktrantype": "1","tranamt": "1","pwdflag": "1","imei": account}},
             "bankStockService.queryBalance":{"dynamicMap": {"sessionId": "?","imei": account,"fundid" : "?", "moneytype": "0","fundpwd": "111111","bankcode": "4","bankpwd": "111111"}},
             "bankStockService.queryBankList":{"dynamicMap": {"sessionId": "8c2e7b68-c693-4f15-8413-98d1cc5e0971","moneytype" : "0", "bankcode": "", "fundid": fundid}},
             "bankStockService.thirdManagement":{"dynamicMap": {"sessionId": "sessionId?","moneytype" : "0", "bankcode": ""}},
             "bankStockService.queryBankTrans":{"dynamicMap": {"sessionId": "sessionId?","fundid" : fundid,"moneytype": "0","sno": ""}}}
    #返回的字典，最后只以key:value的形式被rf接受
    allreturndict = {'TRADE_DEFAULT_DIC':TRADE_DEFAULT_DIC1}
    allreturndict.update(account_map[account])
    return allreturndict


# #quotation
# QUOTATION_ITEM={"api.thrift.quotation.security.query":["code","currency","engName","index","listedDate","listedStatus","lotsize","market","name","preName","precision","stockType","tradeType","type","updateTime"],"api.thrift.quotation.realtime":["amount","amplitude","buylist","buylistIterator","buylistSize","code","floating","floatingValue","inDish","lastClose","max","min","open","outDish","price","profit","qty","selllist","selllistIterator","selllistSize","setAmount","setAmplitude","setBuylist","setCode","setFloating","setFloatingValue","setInDish","setLastClose","setMax","setMin","setOpen","setOutDish","setPrice","setProfit","setQty","setSelllist","setStatus","setTime","setTotal","setTotalValue","setTp","setTurnoverRate","status","time","total","totalValue","tp","turnoverRate"],"api.thrift.quotation.security.queryAll":["baseSecurity","code","currency","engName","index","listedDate","listedStatus","lotsize","market","name","preName","precision","stockType","tradeType","type","updateTime"]}
# QUOTATION_CHECK_LIST={"api.thrift.quotation.security.query":"securityList",
#                       "api.thrift.quotation.realtime":"quotationList",
#                       "api.thrift.quotation.simple":"simpleQuotationList",
#                       "api.thrift.quotation.timeTrend.query":"quotationList",
#                       "api.thrift.quotation.security.queryAll":"securityList"}



