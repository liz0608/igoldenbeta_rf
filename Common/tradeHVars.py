#!/usr/bin/env python
# -*- coding: utf-8 -*-

#10.31.90.109 9999 10.31.90.102 1090 121.40.156.222 9090
# SocketIp='10.31.90.102'
# IntSocket=1090
# Version='V_1_0'
#HK stock
def get_variables(env='test'):
    if env=='product':
        phone = '17704027142'
        variables = {
            'tradeUrl' : 'https://mtrade.igoldenbeta.com',
            'tradeUrl_xct' : 'https://120.78.24.195:8449',
            'tradeWebUrl':"https://mtrade.igoldenbeta.com/html/app.html",
            'fundid': getTradeInfos(phone)[0],
            'trdpwd': getTradeInfos(phone)[1],
            'secuid': getTradeInfos(phone)[2],
            'TRADE_DEFAULT_DIC' : TRADE_DEFAULT_DIC(phone),
        }
    else:
        phone = '15000000021'
        variables = {
            'tradeUrl' : 'https://10.31.90.102:8559',
            'tradeWebUrl':"https://10.31.90.102:8559/html/app.html",
            'fundid': getTradeInfos(phone)[0],
            'trdpwd': getTradeInfos(phone)[1],
            'secuid': getTradeInfos(phone)[2],
            'TRADE_DEFAULT_DIC' : TRADE_DEFAULT_DIC(phone)
        }
    variables['securitiesCode'] = '06'

    return variables

def getTradeInfos(phone):
    account_map ={
        #test
        "66666661":{"fundid":"TEST015","trdpwd":"123abc","secuid":"TEST015"},
        "13100001112":{"fundid":"TEST0810001","trdpwd":"abc123","secuid":"TEST0810001"},
        #production
        "13066839826":{"fundid":"TEST002","trdpwd":"Abc123","secuid":"TEST002"},
        "17704027142":{"fundid":"TEST003","trdpwd":"abcd1234","secuid":"TEST003"},
        "15000000021":{"fundid":"TEST1","trdpwd":"pbwsyqds","secuid":"TEST1"},
        }
    return account_map[phone]['fundid'],account_map[phone]['trdpwd'],account_map[phone]['secuid']

#trade
def TRADE_DEFAULT_DIC(account):
    fundid = getTradeInfos(account)[0]
    trdpwd = getTradeInfos(account)[1]
    secuid = getTradeInfos(account)[2]

    TRADE_DEFAULT_DICIN={
                 "customerService.getVerificationCode":{"dynamicMap": {"imei": account, "debug" : "True"}},#yes
                 "customerService.isShowAgreement":{},#no need
                 "customerService.getAccountStatus":{"dynamicMap": {"mobileNo":account}},
                 "customerService.login":{"dynamicMap": {"inputtype": "C","inputid": fundid,"trdpwd": trdpwd,"verifyCode": "","imei": account}}, #yes, pri 30min
                 "customerService.logout":{"dynamicMap": {"sessionId": "sessionId?", "imei": account}}, #no need
                 "queryService.queryFund":{"dynamicMap": {"sessionId": "","fundid": fundid,"moneytype": 1}}, #yes
                 "queryService.queryStock":{"dynamicMap": {"sessionId": "sessionId?","fundid": fundid,"qryflag": "1","count": "15","stkcode": "","market": "4","poststr": "","moneytype":1}},#yes
                 "queryService.queryOrders":{"dynamicMap": {"securitiesCode":"06","sessionId":"","fundid":fundid,"qryflag":"1","count":"","poststr":"","cancelflag":"0","stkcode":"","market":"","istoday":"1","begindate":"","enddate":""}},
                 "queryService.queryDone":{"dynamicMap": {"sessionId": "sessionId?","fundid": fundid,"qryflag": "1","count": "1000","securitiesCode": "06"}},#接口已经废弃,鑫财通需要
                 #"queryService.getMaxTranseCount":{"dynamicMap": {"sessionId": "sessionId?","market": "0","secuid": secuid,"fundid": fundid,"stkcode": "000010","bsflag": "0B","price": "6.86"}},
                 #"queryService.queryAvailableFund":{"dynamicMap": {"sessionId": "sessionId?","fundid": fundid,"moneytype": 1}}, #yes
                 "queryService.queryMatch":{"dynamicMap": { "sessionId" : "sessionId?", "pattern" : "pattern?" }},  #yes
                 "customerService.getSubaccountInfo":{"dynamicMap":{"brokerageCusCode":fundid}}, #yes
                 "customerService.getsubaccountinfo":{"dynamicMap":{"brokerageCusCode":fundid}},
                 "queryService.queryMarket":{"dynamicMap": { "sessionId" : "sessionId?", "code" : "code?","market":"4"}},#yes
                 "queryService.queryMatchExact":{"dynamicMap": { "sessionId" : "sessionId?", "stkcode" : "code?","market": "4" }},#no
                 "queryService.queryQuotationCredit":{"dynamicMap":{"sessionId":"98c6d5f0-f410-4133-9568-d4250745906a"}},#not need
                 "tradeService.chechBilker":{"dynamicMap":{"secuEncode":"00260.HK","sessionId":"fa7d5269-a7ba-4848-8c63-af7b3d2abf47"}}, #yes
                 "tradeService.confirmTrans":{"dynamicMap": {"sessionId": "sessionId?","fundid": fundid,"secuid": secuid,"stkcode": "000011","market": "4",
                                                             "bsflag": "0B","price": "11","qty": "1000","ordergroup": "0","exchtype":"0"}},
                 "tradeService.cancelTrans":{"dynamicMap": {"sessionId": "sessionId?","fundid": fundid,"ordersno": ""}}, #no
                 "tradeService.changeTrans":{"dynamicMap": {"sessionId": "sessionId?","fundid": fundid,"ordersno": "","price": "1.48","qty": "1000"}},
                 "mobileTransService.verfyLogin": {"dynamicMap": {"imei": account,"uid":"101216"}}, #not need
                 "customerService.queryUserInfo":{"dynamicMap": {"sessionId": "sessionId?", "custid": fundid}},#yes
                 "customerService.modifyUserInfo":{"dynamicMap": {"sessionId": "sessionId?", "custid": fundid,"email": "","telephone": "13066839826"}},#yes
                 "customerService.forgotPassword":{"dynamicMap": {"sessionId": "sessionId?", "custid": fundid,"name": "name?", "idcardno": "idcardno?","email": "","telephone": "3420"}},#no need
                 "bankStockService.transferSecurityToBank":{"dynamicMap": {"sessionId": "sessionId?", "fundid": fundid,"tranamt":"10","cur":1}}, #yes
                 "bankStockService.queryBankSecuTrans":{"dynamicMap": {"sessionId": "sessionId?", "fundid": fundid}}, #yes
                 "bankStockService.queryBankList":{"dynamicMap": {"sessionId": "sessionId?", "fundid": fundid}}, #yes
                 #"bankStockService.transferBankToSecurity":{}
                 "api.system.stock.stagging.list":{"data": {"type": "0"}, "v": "1.0"},
                "api.system.stock.stagging.itemlist":{"data": {}, "v": "1.0"},
                "api.system.stock.stagging.spend":{"data": {}, "v": "1.0"},
                "api.system.stock.stagging.subscrib":{"data": {"fundId": "", "userName": "", "code": "", "uid": "", "totalSpend": "", "shareNum": "1000", "serviceCharge": "50"}, "v": "1.0"},
                "api.system.stock.stagging.historylist":{"data": {"fundId":fundid}, "v": "1.0"},
                 "bankStockService.getBankList":{"data": {"fundId":fundid}, "v": "1.0"},

    }

    #返回的字典，最后只以key:value的形式被rf接受
    #allreturndict = {'TRADE_DEFAULT_DIC':TRADE_DEFAULT_DICIN,'fundid':fundid,'trdpwd':trdpwd,'secuid':secuid}

    return TRADE_DEFAULT_DICIN





