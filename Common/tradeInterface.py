#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from thrift.protocol.TMultiplexedProtocol import TMultiplexedProtocol
# sys.path.append('./ThriftServer')

from ThriftServer.cn.jsfund.thrift.mvc import Bkt
from ThriftServer.cn.jsfund.thrift.mvc.ttypes import *
from thrift.transport import TSocket
from thrift.transport import TSSLSocket
from thrift.transport import TTransport
from thrift.protocol import TCompactProtocol

from robot.api import logger


def objToDict(obj):
    '''把Object对象转换成Dict对象'''
    dict = {}
    dict.update(obj.__dict__)
    return dict

def thriftInterface(socketIp,intSocket,version,api,adic):
    '''通过client向Thrift发送api请求'''
    #original input data
    #inputResult=inputDataOprate(exceldata,adic)
    #if not inputResult:
    #    return False
    transport = TSocket.TSocket(socketIp,intSocket)
    transport = TTransport.TBufferedTransport(transport)
    protocol = TCompactProtocol.TCompactProtocol(transport)
    p = TMultiplexedProtocol(protocol, "bkt")

    client = Bkt.Client(p)

    transport.open()

    request=Request()
    request.api=api
    request.version=version
    request.data=json.dumps(adic,ensure_ascii=False)
    try:
        output=client.api(request)
        output=objToDict(output)
        transport.close()
        return True, output
    except Exception,e:
        logger.info("input:%s" %adic)
        logger.info("thrift_error:%s" %str(e._message).decode('utf-8'))
        transport.close()
        return False, str(e._message).decode('utf-8')

# SocketIp='10.31.90.102'
# IntSocket=1090
# Version='V_1_0'

# SocketIp='10.31.90.104'
# IntSocket=9090
# Version='V_1_0'
# api = 'adjustDataService.getAdjustData'
# adic = {"portfolioId":151}
# data = thriftInterface(SocketIp, IntSocket,Version,api,adic)
# print data
# api = 'mobileTransService.verfyLogin'
# adic = {'CommonRequest': {'userId': '101213', 'dynamicMap': {'imei': '13100001112'}, 'securitiesCode': '06'}}
#
# data = thriftInterface(SocketIp, IntSocket,Version,api,adic)
# print data
# outdata = data[1].encode("utf-8")
# print  outdata
# vercode = outdata['data']['verifyCode']
#
# print(vercode)
# apilogin = "customerService.login"
# logindic = {"CommonRequest": {"userId": "101213","securitiesCode": "06","dynamicMap": {"inputtype": "C","inputid": "1000016","trdpwd": "123abc","verifyCode": vercode,"imei": "13100001112","debug":"true"}}}
#
# outputlogin = thriftInterface(SocketIp, IntSocket,Version,logindic,apilogin)
#
# print outputlogin