*** Settings ***
Library           RequestsLibrary
Library           YamlLibrary
Library           Collections
Resource          session及请求.txt
Library           String
Variables         tradeAVars.py    02
Library           tradeInterface.py
Library           readtemp.py

*** Variables ***
${SocketIp}       10.31.90.102
${IntSocket}      1090
${Version}        V_1_0

*** Keywords ***
tradeA_request
    [Arguments]    ${secuCode}    ${api}    ${dynamicData}={}    ${uid}=${truid}    ${returnlist}=
    [Documentation]    原始的Thrift入参dict长这样：
    ...
    ...    {"customerService.getVerificationCode":{"CommonRequest": {"userId": "101213","securitiesCode": "06","dynamicMap": {"imei": "13100001112", "debug" : "true"}}}}
    ...
    ...    初始化时组合数据
    ...
    ...    返回数据长这样：["message":"请求成功, 耗时：16毫秒","data":"{"status":"1","message":"请求成功","data":{"verifyCode":"9118"}}"]
    ...
    ...    返回message及data，data以字符串的形式返回
    log    正在请求接口：${api}
    ${reqData}    Set Variable    ${TRADE_DEFAULT_DIC['${api}']}
    ${length}    Get Length    ${reqData}
    ${new}    Run Keyword If    ${length}>0    Update Dictionary    ${reqData['dynamicMap']}    ${dynamicData}
    Set To Dictionary    ${reqData}    securitiesCode=${secuCode}    userId=${uid}
    ${apiData}    Create Dictionary    CommonRequest=${reqData}
    ${result}    Thrift Interface    ${SocketIp}    ${${IntSocket}}    ${Version}    ${api}    ${apiData}
    Run Keyword If    ${result[0]} == False    fail    接口返回错误：${result[1]}
    ${retMessage}    Set Variable    ${result[1]['message'].decode('utf-8')}
    ${retData}    Set Variable    ${result[1]['data'].decode('utf-8')}
    #处理返回数据
    ${retlen}    get length    ${returnlist}
    Comment    ${resultarr}    Run Keyword If    '${retlen}'=='0'    Set Variable    ${retData}
    ...    ELSE    取返回值    ${returnlist}    ${retData}
    ${resultarr}    Run Keyword If    '${retlen}'=='0'    Evaluate    [${retMessage},${retData}]
    ...    ELSE    取返回值    ${returnlist}    ${retData}
    [Return]    ${resultarr}

指定账号登录并返回sessionid

国信默认账号登录
    ${returnlist}    Create List    data.verifyCode
    ${retCode}    tradeA_request    01    customerService.getVerificationCode    returnlist=${returnlist}
    ${returnlist1}    Create List    data.0.SESSION
    ${retlogin}    tradeA_request    01    customerService.login    {"verifyCode":"${retCode[0]}"}    returnlist=${returnlist1}
    ${sessionid}    Set suite Variable    ${retlogin[0]}
