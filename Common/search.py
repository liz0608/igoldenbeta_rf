# coding=utf-8
import random
import string
def search():
	one_key = ''.join([random.choice(string.digits) for key in range(1)])
	two_key = ''.join([random.choice(string.digits) for key in range(2)])
	three_key = ''.join([random.choice(string.digits) for key in range(3)])
	four_key = ''.join([random.choice(string.digits) for key in range(4)])
	five_key = ''.join([random.choice(string.digits) for key in range(5)])
	six_key = ''.join([random.choice(string.digits) for key in range(6)])
	one_chars = ''.join([random.choice(string.ascii_letters) for key in range(1)])
	two_chars = ''.join([random.choice(string.ascii_letters) for key in range(2)])
	three_chars = ''.join([random.choice(string.ascii_letters) for key in range(3)])
	keyword_list = [one_key,two_key,three_key,four_key,five_key,six_key,one_chars,two_chars,three_chars]
	return keyword_list
