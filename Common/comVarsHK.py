#!/usr/bin/env python
# -*- coding: utf-8 -*-

from time import strftime, localtime
pnTime = strftime("%Y",localtime())+strftime("%m",localtime())+strftime("%d",localtime())+strftime("%H",localtime())\
         +strftime("%M",localtime())+strftime("%S",localtime())

def get_variables(env='test'):
    if env=='product':
        variables = {
            "url_http": 'http://www.igoldenbeta.com:8080',
            "url_https": 'https://www.igoldenbeta.com:8443',
            "url_quotation":"https://121.43.79.187:8443",
            "loginPhone": "17704027142",
            "loginPw":'111111',
            "trdpwd":'abcd1234',
            "otherPhone":  "13265829651",
            "otherPhone2":  "13100001118",
            "otherPhone3":  "13100000001",
            "betaPhone":  '18888888888',
            "smsPhone":  "14000000001",
            #dbConfig
            "dbSmsUser":"smstest",
            "dbSmsPw":"smstest_2015",
            "dbSmsName":"sms_test",
            "dbMobileUser":"jsfundtest",
            "dbMobilePw":"hsoffice0755",
            "dbHost" : "116.62.39.108",
            "dbPort":13506,
            #manage
            "domainManage":"http://121.40.35.116:8081/cn-jsfund-client-manage"
        }

    elif env=='exp':
        variables = {
            "url_http": 'http://121.40.35.116:8080',
            "url_https": 'https://121.40.35.116:8443',
            "url_quotation":"https://121.43.79.187:8443",
            "loginPhone": "13066839826",
            "loginPw" : '123456',
            "otherPhone":  "13265829651",
            "otherPhone2":  "13100001118",
            "otherPhone3":  "13100000001",
            "betaPhone":  '18888888888',
            #dbConfig
            "dbSmsUser":"smstest",
            "dbSmsPw":"smstest_2015",
            "dbSmsName":"sms_test",
            "dbMobileUser":"jsfundtest",
            "dbMobilePw":"hsoffice0755",
            "dbHost" : "116.62.39.108",
            "dbPort":13506,
            #manage
            "domainManage":"http://121.40.35.116:8081/cn-jsfund-client-manage"
        }
    else:
        variables = {
            "url_http": 'http://116.31.80.124:8081',
            "url_https": 'https://116.31.80.124:8444',
            "url_quotation":"https://121.43.79.187:8443",
            "loginPhone": "15000000021",
            "loginPw":'111111',
            "trdpwd":'pbwsyqds',#abc123
            "otherPhone":  "13100001113",
            "otherPhone2":  "13100001118",
            "otherPhone3":  "13100000001",
            "betaPhone":  '18888888888',
            "smsPhone":  "14000000001",
            "verifySmsPhone": "14400000001",
            "registerPhone":  "14200000001",
            "modifyPhone": "14300000001",
            "resetPwPhone ": "14400000001",
            #dbConfig
            "dbSmsUser":"root",
            "dbSmsName":"jsfund_sms_dev",
            "dbSmsPw" :"!2D#34S3aA$",
            "dbMobileUser":"root",
            "dbMobilePw" :"!2D#34S3aA$",
            "dbHost" : "10.31.90.118",
            "dbPort":3306,
            #manage
            "domainManage":"http://10.31.74.101:8080/cn-jsfund-client-manage"
        }
    variables.update({"pnTime":pnTime})

    return variables

MOBILE_DEFAULT_VALUE={
        #user
        "api.system.user.sms":{"v":"1.0","data":{"phone" : "","type" : "1"}},
        "api.system.user.verify.sms":{"v":"1.0","data":{"phone":"",'smsvcode':'111111'}},
        "api.system.user.register":{"protocol":"https","v":"1.0","data":{"phone" : "", "bg" : "2",
                                        "smsvcode" : "1111","nickname" : "AutoNick001",
                                        "pw" : "81a9675be670bdc97c7f57b0d4191a39","alerttype" : "1"}},
        "api.system.user.login":{"protocol":"https","v":"1.0","data":{"phone" : "","pw" : "81a9675be670bdc97c7f57b0d4191a39",
                                                                      "token":"null","thirduid":"null","expire":"null"}},
        "api.system.user.validate.session":{"v":"1.0","data":{}},
        "api.system.user.logout":{"protocol":"https","v":"2.0","data":{"uid" : "106","outerToken" : "7d597d89 6…f6d0","innerToken":""}},
        "api.system.user.modify.pic":{"v":"1.0","data":{"picdata":"\/9j\/4A…jwaq","uid":"1","suffix":"png"}},
        "api.system.user.modify.userinfo":{"v":"1.0","data":{"uid":"1","alerttype":"1","nickname":"AutoModify","sex":"0",
                                                             "desc":"AutoDiscipt","bg":"1","alerttype":"0","email":"null"}},
        "api.system.user.modify.phone":{"v":"1.0","data":{"uid":"1","phone":"","pw":"81a9675be670bdc97c7f57b0d4191a39","smsvcode":"1111"}},
        "api.system.user.verify.pw":{"protocol":"https","v":"1.0","data":{"uid":"1","pw":"81a9675be670bdc97c7f57b0d4191a39"}},
        "api.system.user.detail":{"v":"1.0","data":{"uid":"1","targetid":"106"}},
        "api.system.user.list":{"v":"1.0","data":{"start":"0","limit":"20","auth":"0"}},
        "api.system.user.follow":{"v":"1.0","data":{"type":"0","uid":"1","followid":"null"}},
        "api.system.user.followlist":{"v":"1.0","data":{"uid":"1","targetid":"106","type":"1","start":"0","limit":"20"}},
        "api.system.user.activity":{"v":"1.0","data":{"uid":"1"}},#3.8更新
        "api.system.user.listhero":{"v":"1.0","data":{"uid":"1"}},
        "api.system.user.reset.pw":{"v":"1.0","data":{"phone":"",'smsvcode':'1111','pw':"ab61…907f0"}},
        "api.system.user.modify.pw":{"v":"1.0","data":{"uid":"1","oldpw":"","newpw":""}},
        "api.system.notify.redpoint":{"v":"1.0","data":{}},
        "api.system.notify.grouplist":{"v":"3.0","data":{"uid":"1"}}, #迭代3.1更新到V3.0
        "api.system.notify.list":{"v":"1.0","data":{"uid":"1","type":"5","start":"0","limit":"20"}},#迭代3.1更新到V3.0
        "api.system.third.verify":{"v":"1.0","data":{"thirduid":"","source":"2","token":"","expire":""}},
        "api.system.third.bind":{"v":"1.0","data":{"uid":"1","thirdid":"","source":"2","type":"1","token":"","expire":""}},
        #basket
        "api.system.basket.listbytype":{"v":"2.0","data":{"sort":"FZ","type":"0","filter":"1","adjusttime":"","createtime":"",
                                                          "assessment":"","start":"0","limit":"20","market":"0"}},
        "api.system.basket.detail":{"v":"4.0","data":{"from":"","foreground":"1","bktid":"1001773480","stockdetail":"0"}}, #3.0更新到4.0版本,3.1更新到5.0版本,3.8版本新增
        "api.system.basket.hold.hkstock":{"v":"1.0","data":{"basketId":"1001773480","codes":"00852.HK,00001.HK"}},#3.0新增
        "api.system.basket.historylist":{"v":"4.0","data":{"basketid":"1001773480","start":"0","limit":"10"}},
        "api.system.basket.adjustlist":{"v":"2.0","data":{"start":"0","limit":"4","uid":""}},
        "api.system.basket.stockdetail":{"v":"1.0","data":{"code":"300221.SZ","category":"2"}},#3.9返回增加yearHighPrice,yearLowPrice字段
        "api.system.basket.hotfavoritelist":{"v":"1.0","data":{"start":"0","limit":"20","uid":"1"}},
        "api.system.basket.favoritelist":{"v":"1.0","data":{"start":"0","limit":"20","uid":"1","targetid":"","orderType":"0","orderWay":"0"}},
        "api.system.stock.favoritelist":{"v":"1.0","data":{"start":"0","limit":"20","uid":"1","targetid":"","orderType":"0","orderWay":"0"}},
        "api.system.stock.favorite":{"v":"1.0","data":{"code":"002063.SZ","uid":"101212","type":"1"}},
        "api.system.basket.favoritestatus":{"v":"1.0","data":{"uid":"1"}},
        "api.system.basket.screen":{"v":"1.0","data":{"screen":"","platform":"1"}},
        "api.system.basket.home.operation":{"v":"2.0","data":{"type":"2","platform":"1"}},  #迭代3.0升级到2.0版本
        "api.system.basket.realtime.index":{"v":"1.0","data":{}},
        "api.system.basket.refreshlist":{"v":"1.0","data":{"bktids":"701,641,391,742,674,867"}},
        "api.system.basket.searchbytype":{"v":"1.0","data":{"limit":"20","type":"1","start":"0","keyword":"0"}}, #迭代3.0升级到4.0版本
        "api.system.basket.nameallow":{"v":"2.0","data":{"name":"minibasket"}},
        "api.system.basket.list.filter":{"v":"1.0","data":{"root":"0","type":"1"}},
        "api.system.basket.compare.base":{"v":"1.0","data":{"market" : "0","name" : "","type" : "0"}},
        "api.system.basket.search.stock":{"v":"1.0","data":{"limit":"20","start":"0","market":"0","name":"h"}},
        "api.system.basket.create":{"v":"2.0","data":{"topics" : "30007","uid" : "1","desccontent" : "","stocks" : "000581.SZ\/0.330000,000538.SZ\/0.330000,000586.SZ\/0.340000",
                                                              "suffix" : "jpg","market" : "0","norm" : "399300.SZ","description" : u"党的领导英明神武","type" : "0",
                                                              "name" : u"测试哦","picdata" : "XXREWQT"}},
        "api.system.basket.update":{"v":"2.0","data":{ "topics" : "30007","uid" : "1","desccontent" : "","norm" : "399300.SZ",
                                                       "basketid" : "1003458684","description" : "look 我想楼下阿卡丽","type" : "0","name" : u"测试哦" }},
        "api.system.basket.updatestatus":{"v":"1.0","data":{"uid":"1","status":"1","basketid":"1002243972"}},
        "api.system.basket.delete":{"v":"1.0","data":{"uid":"1","basketid":"1002243972"}},
        "api.system.basket.search":{"v":"4.0","data":{"keyword":"ha"}},  #3.0更新到5.0版本
        "api.system.stock.index.stockrank":{"v":"1.0","data":{"limit":"20","rankWay":"1","code":"000001.SH","start":"0","ranking":"0"}},
        "api.system.basket.favorite":{"v":"1.0","data":{"type":"1","uid":"1","basketid":"145"}},
        "api.system.basket.stockadjust":{"v":"1.0","data":{"basketid":"1002235122"}},
        "api.system.basket.comment":{"v":"1.0","data":{"content":"great","uid":"1","basketid":"1002235122","top":"0"}},
        "api.system.basket.comment.list":{"v":"2.0","data":{"order":"1","count":"10","basketid":"1002235122","cursor":"-1"}},
        "api.system.basket.chart":{"v":"3.0","data":{"type":"4","bktid":"145"}},
        "api.system.basket.statuslist":{"v":"2.0","data":{"uid":"1"}}, #3.3更新入参新增type字段，1：获取迷你组合列表，不传或者空获取所有
        "api.system.basket.article.comment":{"v":"1.0","data":{"articleid":"1","content":"","targetid":"","commentid":""}},#2.8新增
        "api.system.basket.article.comment.delete":{"v":"1.0","data":{"commentid":"","uid":""}},
        "api.system.basket.article.comment.list":{"v":"1.0","data":{"articleid":"","cursor":"-1","count":"20"}},
        "api.system.cms.article.todaytopic":{"v":"1.0","data":{"limit":"20","start":"0"}},
        "api.system.cms.article.foranalysit":{"v":"1.0","data":{"limit":"20","start":"0"}},
        "api.system.basket.banner":{"v":"1.0","data":{"position":"4"}},  #3.0新增
        "api.system.basket.favorite.move":{"v":"1.0","data":{"uid":"","fromid":"","toid":""}},
        #"api.system.basket.favorite.top":{"v":"1.0","data":{"type":"0","targetid":"${uid}"}},
        "api.system.stock.favorite.move":{"v":"1.0","data":{"uid":"","fromid":"","toid":""}},
        #"api.system.stock.favorite.top":{"v":"1.0","data":{"uid":"","fromid":"","toid":""}},
        "api.system.basket.hot.theme":{"v":"1.0","data":{"homepage":"0","start":"0","limit":"20"}},
        "api.system.basket.high.earnings":{"v":"1.0","data":{"homepage":"0","start":"0","limit":"20"}},
        "api.system.basket.low.drawdown":{"v":"1.0","data":{"homepage":"0","start":"0","limit":"20"}},
        "api.system.basket.hot.expert":{"v":"1.0","data":{"homepage":"0","start":"0","limit":"20"}},
        "api.system.basket.hot.talent":{"v":"1.0","data":{"homepage":"0","start":"0","limit":"20"}},
        "api.system.basket.goldbetagrade":{"v":"1.0","data":{"bktid":"141"}},
        "api.system.basket.buygoldbeta":{"v":"1.0","data":{"gradeid":""}},
        "api.system.cms.article.investlecture":{"v":"1.0","data":{"start":"0","limit":"20"}},
        #common
        "api.system.common.beta.const":{"v":"1.0","data":{}},
        "api.system.common.update":{"v":"1.0","data":{"platform":"1","version":"23"}},
        "api.system.common.config":{"v":"1.0","data":{"platform":"1","version":"23","uid":"1"}},
        "api.system.common.statistics":{"v":"1.0","data":{"platform":"1"}},
        "api.system.common.push":{"v":"1.0","data":{"token":"","uid":"1"}},
        "api.system.common.fileupload":{"uri":"common/files","v":"1.0","data":{"file":"","module":"1"}},
        "api.system.common.setting":{"v":"1.0","data":{"uid":"1","push":"1","pushbegin":"null","pushend":"null",
                                                       "commentpush":"0","atpush":"0","comment":"0"}},
        "api.system.common.setting.detail":{"v":"1.0","data":{"uid":"1"}},
        "api.system.common.uploadpush":{"v":"2.0","data":{"pushid":"","platform":"0","channel":"0","uploadtype":"0"}},#3.3更新到2.0接口
        "api.system.push.register":{"v":"1.0","data":{"token":"","version":"","timestamp":pnTime}},
        "api.system.common.ad":{"v":"1.0","data":{}},  #2.8新增
        "api.system.common.acceptshare":{"v":"1.0","data":{"shareId":"","targetPhone":""}},#2.8新增
        "api.system.common.sendshare":{"v":"1.0","data":{}},#2.8新增,取登录的sid做账号校验
        "api.system.common.sharelist":{"v":"1.0","data":{"uid":"101621","start":"0","limit":"4"}},#2.8新增
        "api.system.common.app.patch":{"v":"1.0","data":{"appversion":"34","fixversion":"","platform":"2"}},


        #feed
        "api.system.feed.list":{"v":"1.0","data":{"order":"0","count":"20","uid":"1","cursor":"-1"}},
        "api.system.feed.post":{"v":"1.0","data":{"content":"chjnhj","uid":"1","refList":'[]',"refcommentid":"","type":"1","media":'[]'}},
        "api.system.basket.comment.delete":{"v":"1.0","data":{"uid":"1","commentid":"15399"}},
        "api.system.feed.comment.list":{"v":"1.0","data":{"cursor" :"","commentid":"16998","count":"20","order" :"0","uid":"101213"}},
        "api.system.feed.detail":{"v":"1.0","data":{"commentid":"15399"}},

        #mini
        "api.system.mini.list":{"v":"2.0","data":{"homepage":"1","start" : "0","limit":"20"}},#3.3更新为2.0，返回值新增字段isBuy是否购买迷你组合，1：是，0：否
        "api.system.mini.detail":{"v":"1.0","data":{"betaid":"1001859113"}},
        "api.system.mini.remind":{"v":"1.0","data":{"betaid":"1001859113","uid":"1","remind":"1"}},#3.3入参新增字段fixedInvestRemind和fixedRemindDate
        "api.system.mini.point.add":{"v":"1.0","data":{"betaid":"1001859113","uid":"1"}},
        "api.system.feed.livebrocast":{"v":"2.0","data":{"cursor":"","count":"20"}}, #3.0更新到2.0版本
        "api.system.mini.point.list":{"v":"1.0","data":{"betaid":"1001859113","uid":"1","start" : "0","limit":"20"}},
        "api.system.mini.jsweb":{"v":"1.0","data":{"uid":"1"}},
        "api.system.mini.banner":{"v":"1.0","data":{}},#2.8新增
        "api.system.common.mygold":{"v":"1.0","data":{}},#2.8新增
        "api.system.common.goldstream":{"v":"1.0","data":{"start":"0","limit":"20"}},#2.8新增
        "api.system.common.goldbetagrade":{"v":"1.0","data":{"bktid":""}},#2.8新增
        "api.system.common.buygoldbeta":{"v":"1.0","data":{"gradeid":""}},#2.8新增
        "api.system.point.goldlist":{"v":"2.0","data":{}},#2.8新增,3.1更新到2.0版本
        "api.system.point.golddetail":{"v":"1.0","data":{"task":""}},#2.8新增
        "api.system.mini.is.point":{"v":"1.0","data":{"uid":""}},
        "api.system.notify.add.dynamic":{"v":"1.0","data":{}},
        "api.system.trade.getTraderList":{"protocol":"https","v":"1.0","data":{"exchangeid":"0"}},#默认A股
        "api.system.basket.netvalue.list":{"v":"1.0","data":{"start":"0","limit":"20"}},
        "api.system.common.trade.desc":{"v":"1.0","data":{}},
        "api.system.feed.comment.top":{"v":"1.0","data":{"commentid":"17230","type":"1","uid":"101213"}},
        "api.system.basket.search.hotwordslist":{"v":"1.0","data":{}},

        "api.system.cgb.list":{"v":"1.0","data":{"type":"1"}},#3.8新增
        "api.system.cgb.souvenir.list":{"v":"1.0","data":{}},#3.8新增
        "api.system.cgb.souvenir.detail":{"v":"1.0","data":{"cgbId":""}},#3.9新增
        "api.system.cgb.souvenir.update":{"v":"1.0","data":{"cgbId":"","thoughts":"foreverlove,gold","buyer":"Atester","buyShares":"100"}},#3.8新增
        "api.system.cgb.comment.list":{"v":"1.0","data":{"cgbId":"","cursor":"-1","count":"30"}},#3.8新增
        "api.system.cgb.comment":{"v":"1.0","data":{"cgbId":"5","content":"test1123"}},#3.8新增
        "api.system.cgb.comment.top":{"v":"1.0","data":{}},#3.8新增
        "api.system.cgb.detail":{"v":"1.0","data":{"cgbId":"5"}},#4.0新增
        "api.system.user.activity":{"v":"1.0","data":{}},#用type区分推广员app入口
        "api.system.mini.invester.list":{"v":"1.0","data":{"homepage":"","type":"2","start":"0","limit":"20"}},#3.8新增
        "api.system.stock.remind.page":{"v":"1.0","data":{"code":"000001.SZ"}},
        "api.system.stock.remind.update":{"v":"1.0","data":{"code":"000001.SZ","category":"2","upTo":"",
                                                            "upToSwitch":"0","downTo":"","downToSwitch":"0",
                                                            "dayDown":"","dayDownSwitch":"0","betaAdjustSwitch":"0"}},#3.9 new
        "api.system.notify.stock.remind.delete":{"v":"1.0","data":{}},#3.9 new
        "api.system.stock.index.stockrank":{"v":"1.0","data":{"code" : "HSI.HK","start" : "0","limit" : "20",
                                                              "ranking":"0","rankWay" : "1"}},#3.9修改，增加ranking类型2:成交额 返回增加 amount，1:涨幅榜 0 跌幅榜  ranking 2成交额榜 ranking 1换手率榜 0涨跌幅
        "api.system.mini.hlinvester.order.list":{"v":"1.0","data":{"start" : "0","limit" : "20"}},#3.9new
        "api.system.mini.hlinvester.code.query":{"v":"1.0","data":{"betaId" : "0","subType" : "0"}},#3.9new,商品类型（0=主组合，其他数字=活动打包组合）
        "api.system.mini.hlinvester.code.check":{"v":"1.0","data":{"betaId" : "0","subType" : "0"}},
        "api.system.basket.api.welcome":{"v":"1.0","data":{}},#4.0new
        "api.system.basket.api.ask":{"v":"1.0","data":{"way":"1","qid":"","question":""}},
        "api.system.basket.api.ask.type":{"v":"1.0","data":{"type":""}},
        "api.system.basket.subject.yuqing":{"v":"1.0","data":{"subjectName":"","start":"0","limit":"20"}},
        "api.system.basket.subject.relatedstock":{"v":"1.0","data":{"subjectName":"","start":"0","limit":"20"}},
        "api.system.basket.subject.relatedinfo":{"v":"1.0","data":{"subjectName":"","start":"0","limit":"20"}},
        "api.system.cms.article.analysttopic":{"v":"1.0","data":{"start":"0","limit":"20"}},
        "api.system.cms.article.officialview":{"v":"1.0","data":{"start":"0","limit":"20"}},
        "api.system.cms.article.list.withauthor":{"v":"1.0","data":{"start":"0","limit":"20"}},
        "api.system.website.bottomad":{"v":"1.0","data":{}},
        "api.system.website.betaratio":{"v":"1.0","data":{}},

        "api.system.account.getpublickey":{"v":"1.0","data":{"uid":"1"}}, #港股
        "api.system.account.login":{"protocol":"https","v":"1.0","data":{"uid":"1","trdpwd":""}},
        "api.system.account.queryfund":{"v":"1.0","data":{"uid":"1","sessionId":""}},
        "api.system.account.querystock":{"v":"1.0","data":{"uid":"1","sessionId":"","qryflag":"0","count":"20"}},
        "api.system.account.queryorders":{"v":"1.0","data":{"uid":"1","sessionId":"","qryflag":"0",
                                                            "count":"20","istoday":"1","begindate":"","enddate":""}}

    }




