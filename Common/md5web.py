#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import hashlib

class md5sum(object):
    def get_sign2(self,api,data):
        #key
        keys="f447da1467a949f0aac6efabf19c6438"
        str=''
        for d in data:
            if str=='':
                str=d+"="+data[d]
            else:
                str = str +"&"+ d+"="+data[d]
        signbefore =  api + "&" + str + "&" +   "kdata=" + keys
        #print "signbefore=" + signbefore

        #sign md5sum
        sign = hashlib.md5()
        sign.update(signbefore)
        sign = sign.hexdigest()
        #print "signafter=" + sign

        data['key']=sign
        print data

        return data

if __name__ == '__main__':
    md5test=md5sum()
    datas={'key22':'12','type':'2'}
    md5test.get_sign2(api='222',data=datas)
