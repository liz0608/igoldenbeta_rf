#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import hashlib

class md5sum(object):
    def get_sign2(self,api,data,t):
        #key
        key="f447da1467a949f0aac6efabf19c6438"

        signbefore =  api + "&" + data + "&" +  "t=" + t + "&" + "kdata=" + key
        #print "signbefore=" + signbefore

        #sign md5sum
        sign = hashlib.md5()
        sign.update(signbefore)
        sign = sign.hexdigest()
        #print "signafter=" + sign

        return sign