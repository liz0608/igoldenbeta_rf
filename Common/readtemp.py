#!/usr/bin/env python
# -*- coding:utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import comVars
import json,redis
from excel import *
from openpyxl.reader.excel import load_workbook

def update_tel_calltimes(host, port, password, tel, time=0):
    try:
        redis_conn = redis.Redis(host=host, port=int(port), db=0, password=password)
    except Exception as ex:
        print("an error raised when connecting to redis:" + str(ex))
        raise Exception(str(ex))
    astr = redis_conn.get ("sms:limit:per:day:" + tel)
    adict = json.loads(astr)
    adict.update({'callTimes':int(time)})
    astr = json.dumps(adict)
    redis_conn.set("sms:limit:per:day:" + tel, astr)
    after_up = redis_conn.get ("sms:limit:per:day:" + tel)
    print("redis has successfully updated :" + tel + ':' + after_up)

def readtempdata(file,sheetName,row):

    rowsNum = excel.excelRowCount(file,sheetName)
    colsNum = excel.excelRowCount(file,sheetName)

    datacollist=[]
    for j in range (1,colsNum):
        data = excel.getCellData(file,sheetName,1,j)
        if 'data' in data:
            datacollist.append(j)
    #print datacollist

    #data build
    lendata = len(datacollist)
    if lendata == 0:
        data = '{}'
    else:
        count=0
        data = '{'
        for j in datacollist:
            datavalue = excel.getCellData(file,sheetName,row,j)
            if datavalue != 'null':
                if count !=0 :
                    data = data + ','
                datakey = excel.getCellData(file,sheetName,1,j)
                datakey = datakey.split('.',1)[1]
                data = data + '"' + datakey + '":"' + excel.getCellData(file,sheetName,row,j) + '"'
                count=count+1
        data = data + '}'

    return data
	



def get_data(parameters=[], datavalues=[]):
    '''
        build the data through the tow input list
        ${data} | get data | ${paras} | ${values}
    '''

    data = {}
    i = 0
    for para in parameters:
        if 'data' in para:
            data[para[5:]] = datavalues[i]
        i = i + 1

    return data
    #if len(data) == 0:
    #    return '{}'
    #else:
    #    strdic = '{"'
    #   strdic = strdic + '","'.join(['":"'.join((k, str(data[k]))) for k in sorted(data, key=data.get, reverse=True)])
    #    strdic = strdic + '"}'
    #    return strdic


def get_parameter(file, sheetName):
    '''
        get the keylist which contains 'data'
        ${data} | get data | ${paras} | ${values}
    '''
    parameter = []
    colsNum = excel.excelColCount(file,sheetName)

    for j in range(colsNum):
        data = excel.getCellData(file,sheetName,1,j)
        if 'data.' in data:
            parameter.append(data)
    return parameter


def change_data(dictIn):
    '''
        change data to a standard form, for example
        ${databefore} | set variable | {"phone":"null","type":"1"}
        ${dataafter} | change data | ${databefore}
        log  | ${dataafter}
        the result is: {"type":"1"}
    '''

    #dictIn = eval(stringIn)
    #print dictIn
    print dictIn
    for key ,value in dictIn.items():
        if value == 'null':
            dictIn.pop(key)

    if len(dictIn) == 0:
        return '{}'
    else:
        strdic = '{"'
        strdic = strdic + '","'.join(['":"'.join((k, str(dictIn[k]))) for k in sorted(dictIn, key=dictIn.get, reverse=True)])
        strdic = strdic + '"}'
        strdic=strdic.replace('"[','[').replace(']"',']')
        return strdic

def write_test_result_data(file, sheetName, data):
    print "data is :"
    print data
    wb = load_workbook(filename = file)
    ws=wb.get_sheet_by_name(sheetName)
    colsNum = excel.excelColCount(file,sheetName)
    num = 0

    for j in range(colsNum):
        cell = excel.getCellData(file,sheetName,1,j)
        if 'PassStatus' == cell:
            num = j

    if num == 0:
        for j in range(colsNum):
            cell = excel.getCellData(file,sheetName,0,j)
            if 'PassStatus' == cell:
                num = j
    print num

    for value in data:
        ws.cell(row = int(value[0]),column =num).value=value[1]
        ws.cell(row = int(value[0]),column =num-1).value=value[2]
        #print value[0],num-1,value[2]
    
    wb.save(filename = file)
    wb = None


def mobile_paras(apiName,dataStr='{}'):
    datadict = comVars.MOBILE_DEFAULT_VALUE[apiName]['data']
    apiVersion = comVars.MOBILE_DEFAULT_VALUE[apiName]['v']
    url = comVars.MOBILE_DEFAULT_VALUE[apiName].has_key('protocol')
    if dataStr == '':
        dataStr = '{}'
    dataNew = eval(dataStr)
    datadictCopy = datadict.copy()
    datadictCopy.update(dataNew)
    if apiName=='api.system.basket.create' or apiName=='api.system.basket.update' or apiName=='api.system.common.attention':
        finalData = change_data(datadictCopy)
    else:
        finalData = json.dumps(datadictCopy,ensure_ascii=False)
    return [url,apiName,apiVersion,finalData]


def update_dictionary(dictOrgin, dataStr='{}'):
    if dataStr == '':
        dataStr = '{}'
    dataNew = eval(dataStr)
    dictOrgin.update(dataNew)

    return dictOrgin

def get_dict_list(listDict, params, changeToNum=0):
    '''
    Example:
    Suppose alist =[{'code':'1'},{'code':'2'},{'code':'3'}]
    ${list}  getDictList  alist  code
    ${list} will be ['1','2','3']
    '''
    if changeToNum == '1':
        return  [float(listDict[i][params]) for i in range(len(listDict))]
    else:
        return  [listDict[i][params] for i in range(len(listDict))]

if __name__ == '__main__':
    update_tel_calltimes('10.31.90.114',6381,'jsfund@local','13100001112')



