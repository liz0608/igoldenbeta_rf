*** Settings ***
Library           RequestsLibrary
Library           YamlLibrary
Library           Collections
Library           String
Variables         tradeHVars.py    ${env}
Library           tradeInterface.py
Library           readtempHK.py

*** Variables ***
${secuCode}       06
${SocketIp}       10.31.90.102
${IntSocket}      1090
${Version}        V_1_0

*** Keywords ***
tradeH_request
    [Arguments]    ${api}    ${dynamicData}={}    ${uid}=${uid}    ${returnlist}=
    [Documentation]    原始的Thrift入参dict长这样：
    ...
    ...    {"customerService.getVerificationCode":{"CommonRequest": {"userId": "101213","securitiesCode": "06","dynamicMap": {"imei": "13100001112", "debug" : "true"}}}}
    ...
    ...    初始化时组合数据
    ...
    ...    返回数据长这样：["message":"请求成功, 耗时：16毫秒","data":"{"status":"1","message":"请求成功","data":{"verifyCode":"9118"}}"]
    ...
    ...    返回message及data，data以字符串的形式返回
    log    正在请求接口：${api}
    ${reqData}    Set Variable    ${TRADE_DEFAULT_DIC['${api}']}
    ${length}    Get Length    ${reqData}
    ${new}    Run Keyword If    ${length}>0    Update Dictionary    ${reqData['dynamicMap']}    ${dynamicData}
    Set To Dictionary    ${reqData}    securitiesCode=${secuCode}    userId=${uid}
    ${apiData}    Create Dictionary    CommonRequest=${reqData}
    ${result}    Thrift Interface    ${SocketIp}    ${${IntSocket}}    ${Version}    ${api}    ${apiData}
    Run Keyword If    ${result[0]} == False    fail    接口返回错误：${result[1]}
    log    ${result}
    ${retMessage}    Set Variable    ${result[1]['message'].decode('utf-8')}
    ${retData}    Set Variable    ${result[1]['data'].decode('utf-8')}
    #处理返回数据
    ${retlen}    get length    ${returnlist}
    ${resultarr}    Run Keyword If    '${retlen}'=='0'    Set Variable    ${retData}
    ...    ELSE    取返回值HK    ${returnlist}    ${retData}
    [Return]    ${resultarr}

指定账号登录并返回sessionid

默认账号登录
    ${retCode}    tradeH_request    customerService.getVerificationCode    returnlist=data.verifyCode
    ${retlogin}    tradeH_request    customerService.login    {"verifyCode":"${retCode[0]}"}    returnlist=data.0.SESSION
    ${sessionid}    Set suite Variable    ${retlogin[0]}
    Set suite Variable    ${sessionid}

tradeH_request_Get
    [Arguments]    ${api}    ${dynamicData}={}    ${sessionId}=    ${uid}=${uid}    ${returnlist}=
    [Documentation]    原始的Thrift入参dict长这样：
    ...
    ...    {"customerService.getVerificationCode":{"CommonRequest": {"userId": "101213","securitiesCode": "06","dynamicMap": {"imei": "13100001112", "debug" : "true"}}}}
    ...
    ...    初始化时组合数据
    ...
    ...    返回数据长这样：["message":"请求成功, 耗时：16毫秒","data":"{"status":"1","message":"请求成功","data":{"verifyCode":"9118"}}"]
    ...
    ...    返回message及data，data以字符串的形式返回
    log    正在请求接口：${api}
    ${reqData}    Set Variable    ${TRADE_DEFAULT_DIC['${api}']}
    ${length}    Get Length    ${reqData}
    ${sessionDict}    Create Dictionary    sessionId=${sessionId}
    ${new}    Run Keyword If    ${length}>0    Update Dictionary    ${reqData['dynamicMap']}    ${dynamicData}    ${sessionDict}
    ${reqData1}    Create Dictionary    dynamicMap=${new}
    Set To Dictionary    ${reqData1}    securitiesCode=${securitiesCode}    userId=${uid}
    ${apiData}    Create Dictionary    CommonRequest=${reqData1}
    ${dataIn}    Evaluate    json.dumps(${apiData})    json
    log    ${dataIn}
    ${random}    Evaluate    ''.join([random.choice(string.letters+string.digits) for i in range(32)])    random,string
    Comment    ${random}    Set Variable    t9jn8fEmpppzlhb2bT6fmhMwoZBLzyMm
    ${params}    Create Dictionary    v=V_1_0    jsoncallback=jsonp    api=${api}    data=${dataIn}    r=${random}
    ${header}    Create Dictionary    Cookie=sid=${sessionId}
    Create Session    tradeH    ${tradeUrl}
    ${resultRes}    Get Request    tradeH    /api/general    params=${params}
    log    ${resultRes.content}
    ${retData}    Evaluate    json.loads($resultRes.json()['data'])    json
    #处理返回数据
    ${retlen}    get length    ${returnlist}
    ${resultarr}    Run Keyword If    '${retlen}'=='0'    Set Variable    ${retData}
    ...    ELSE    取返回值HK    ${returnlist}    ${retData}
    [Return]    ${resultarr}

取返回值HK
    [Arguments]    ${returnlist}    ${resp}
    ${returnlist1}    Run Keyword If    isinstance($returnlist, unicode)    split string    ${returnlist}    ,
    ...    ELSE    Set Variable    ${returnlist}
    ${retlen}    Get Length    ${returnlist1}
    ${returnResult}    create list
    : FOR    ${i}    IN RANGE    ${retlen}
    \    ${re}    get tree    ${resp}    ${returnlist1[${i}]}
    \    Append To List    ${returnResult}    ${re}
    [Return]    ${returnResult}

tradeH_request_Get_cp
    [Arguments]    ${api}    ${dynamicData}={}    ${sessionId}=    ${uid}=${uid}    ${returnlist}=
    [Documentation]    原始的Thrift入参dict长这样：
    ...
    ...    {"customerService.getVerificationCode":{"CommonRequest": {"userId": "101213","securitiesCode": "06","dynamicMap": {"imei": "13100001112", "debug" : "true"}}}}
    ...
    ...    初始化时组合数据
    ...
    ...    返回数据长这样：["message":"请求成功, 耗时：16毫秒","data":"{"status":"1","message":"请求成功","data":{"verifyCode":"9118"}}"]
    ...
    ...    返回message及data，data以字符串的形式返回
    log    正在请求接口：${api}
    ${reqData}    Set Variable    ${TRADE_DEFAULT_DIC['${api}']}
    ${length}    Get Length    ${reqData}
    ${sessionDict}    Create Dictionary    sessionId=${sessionId}
    ${new}    Run Keyword If    ${length}>0    Update Dictionary    ${reqData['dynamicMap']}    ${dynamicData}    ${sessionDict}
    ${reqData1}    Create Dictionary    dynamicMap=${new}
    Set To Dictionary    ${reqData1}    securitiesCode=${securitiesCode}    userId=${uid}
    ${apiData}    Create Dictionary    CommonRequest=${reqData1}
    ${dataIn}    Evaluate    json.dumps(${apiData})    json
    log    ${dataIn}
    Comment    ${random}    Evaluate    ''.join([random.choice(string.letters+string.digits) for i in range(32)])    random,string
    ${random}    Set Variable    t9jn8fEmpppzlhb2bT6fmhMwoZBLzyMm
    ${params}    Create Dictionary    v=V_1_0    jsoncallback=jsonp    api=${api}    data=${dataIn}    r=${random}
    ${header}    Create Dictionary    Cookie=sid=${sessionId}
    Create Session    tradeH    ${tradeUrl}
    ${resultRes}    Get Request    tradeH    /api/general    params=${params}
    log    ${resultRes.content}
    Run Keyword If    '${resultRes.content}' == ''    fail    接口返回错误：没有返回值
    ${resultJson}    Evaluate    json.loads($resultRes.text.split('( ')[1].split(' )')[0])    json
    Run Keyword If    $resultJson['result'] == 'false'    fail    接口返回错误：${result['message']}
    log    ${resultJson}
    ${retData}    Evaluate    json.loads($resultJson['data'])    json
    #处理返回数据
    ${retlen}    get length    ${returnlist}
    ${resultarr}    Run Keyword If    '${retlen}'=='0'    Set Variable    ${retData}
    ...    ELSE    取返回值HK    ${returnlist}    ${retData}
    [Return]    ${resultarr}

tradeH_request_Get_xct
    [Arguments]    ${api}    ${dynamicData}={}    ${sessionId}=    ${uid}=${uid}    ${returnlist}=
    [Documentation]    原始的Thrift入参dict长这样：
    ...
    ...    {"customerService.getVerificationCode":{"CommonRequest": {"userId": "101213","securitiesCode": "06","dynamicMap": {"imei": "13100001112", "debug" : "true"}}}}
    ...
    ...    初始化时组合数据
    ...
    ...    返回数据长这样：["message":"请求成功, 耗时：16毫秒","data":"{"status":"1","message":"请求成功","data":{"verifyCode":"9118"}}"]
    ...
    ...    返回message及data，data以字符串的形式返回
    log    正在请求接口：${api}
    ${reqData}    Set Variable    ${TRADE_DEFAULT_DIC['${api}']}
    ${length}    Get Length    ${reqData}
    ${sessionDict}    Create Dictionary    sessionId=${sessionId}
    ${new}    Run Keyword If    ${length}>0    Update Dictionary    ${reqData['dynamicMap']}    ${dynamicData}    ${sessionDict}
    log    ${new}
    Comment    ${reqData1}    Create Dictionary    dynamicMap=${new}
    Comment    Set To Dictionary    ${reqData1}    securitiesCode=${securitiesCode}    userId=${uid}
    Comment    ${apiData}    Create Dictionary    CommonRequest=${reqData1}
    ${dataIn}    Evaluate    json.dumps(${new})    json
    Comment    log    ${dataIn}
    Comment    ${random}    Evaluate    ''.join([random.choice(string.letters+string.digits) for i in range(32)])    random,string
    Comment    Comment    ${random}    Set Variable    t9jn8fEmpppzlhb2bT6fmhMwoZBLzyMm
    ${params}    Create Dictionary    v=V_1_0    api=${api}    data=${dataIn}    ip=10.31.30.142    channel=jbt
    Comment    Comment    ${params}    Create Dictionary    v=V_1_0    inputtype=C    api=${api}
    ...    inputid=TEST002    trdpwd=Abc123    ip=10.31.30.142    channel=jbt    imei=13066839826
    ${header}    Create Dictionary    Cookie=sid=${sessionId}
    Create Session    tradeH    ${tradeUrl_xct}
    Comment    ${resultRes}    Get Request    tradeH    /api/general    params=${params}
    ${resultRes}    Get Request    tradeH    /api/gbt/hk    params=${params}
    log    ${resultRes.text}
    Comment    Run Keyword If    $resultRes.json()['status'] == '0'    fail    接口返回错误：${resultRes.json()['message']}
    ${retData}    Set Variable    ${resultRes.json()}
    #处理返回数据
    ${retlen}    get length    ${returnlist}
    ${resultarr}    Run Keyword If    '${retlen}'=='0'    Set Variable    ${retData}
    ...    ELSE    取返回值HK    ${returnlist}    ${retData}
    [Return]    ${resultarr}
