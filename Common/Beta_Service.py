#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import time
import urllib
import urllib2
import hashlib
import ssl
import json

class Beta_Service(object):

    def __init__(self):
        self.app_key = "29129215"
        self.app_secret = '57f27786442bc2c946780746c8baa0c6'
        self.ttid = 'Android_5.0_SM-G9006V_igoldenbeta_WIFI_2.4.0.0002_v21'
        self.imei = '352621062275554'
        self.imsi = ''
        self.cid = 'igoldenbeta'
        self.t = int(time.time())
        self.t = str(self.t)
        self.result = {}

    def app_md5(self, data):
        data_md5 = hashlib.md5()
        data_md5.update(data)
        return data_md5.hexdigest()

    def app_sign(self, post_data):
        # key string
        '''
        imei = "73578094f6480ea4ac6bc710c7aae10d"
        imsi = "73578094f6480ea4ac6bc710c7aae10d"
        ttid = "iPhone OS_9.3_iPhone_basket_AppStore_wifi_20160419194655139,73578094f6480ea4ac6bc710c7aae10d,8632_2.4.0_v5438"
        '''
        # appkey md5
        appkey_md5 = self.app_md5(post_data['appKey'])

        # data md5sum
        data_md5 = self.app_md5(post_data['data'])

        signbefore = self.app_secret + "&" + \
                     appkey_md5 + "&" + \
                     post_data['api'] + "&" + \
                     post_data['v'] + "&" + \
                     self.imei + "&" + \
                     self.imsi + "&" + \
                     data_md5 + "&" + \
                     post_data['t']
        #print '签名前:' + signbefore

        # sign md5sum
        sign = self.app_md5(signbefore)
        #print "签名后:" + sign

        return sign

    def encode_multipart_formdata(self, fields={}, files=[], name_para='file'):
        """ 
        fields is a sequence of (name, value) elements for regular form fields. 
        files is a sequence of (name, filename, value) elements for data to be uploaded as files 
        Return (content_type, body) ready for httplib.HTTP instance 
        """ 
        #BOUNDARY = mimetools.choose_boundary() 
        BOUNDARY = "UHQmGBxP2bDl7MBPCsIO61ix4dk8Yu"
        CRLF = '\r\n' 
        L = []
        for (key, value) in fields.items():
            if value == 'null':
                continue
            value = value.encode("utf-8")
            L.append('--' + BOUNDARY) 
            L.append('Content-Disposition: form-data; name="%s"' % key)
            L.append('Content-Type: text/plain; charset=UTF-8')
            L.append('') 
            L.append(value) 
        for filename in files: 
            if filename == 'null':
                continue
            filename = filename.encode("utf-8")
            fr=open(filename,'rb')
            L.append('--' + BOUNDARY) 
            L.append('Content-Disposition: form-data; name="%s"; filename="%s"' %(name_para, filename.split("\\")[-1]))
            L.append('Content-Type: application/octet-stream')
            L.append('') 
            L.append(fr.read())
            fr.close()
        L.append('--' + BOUNDARY + '--') 
        L.append('') 
        body = CRLF.join(L) 
        content_type = 'multipart/form-data; boundary=%s' % BOUNDARY
        print body
        return content_type, body 

    def post_data(self, url, api, version='1.0', data='', sid='', fileupload='', module='', sign='', timestamps='', ttid=''):
        #server_url = 'http://10.31.74.103:8081/cn-jsfund-server-mobile/bkt/api'
        server_url = url.encode("utf-8")
        post_data = {}
        '''
        post_data['imsi'] = '73578094f6480ea4ac6bc710c7aae10d'
        post_data['deviceId'] = '73578094f6480ea4ac6bc710c7aae10d'
        post_data['type'] = 'originaljson'
        post_data['cid'] = 'AppStore'
        post_data['imei'] = '73578094f6480ea4ac6bc710c7aae10d'
        post_data['ttid'] = 'iPhone OS_9.2_iPhone_basket_AppStore_wifi_20160419194655139,73578094f6480ea4ac6bc710c7aae10d,8632_2.4.0_v5438'
        '''
        post_data['imsi'] = self.imsi
        post_data['cid'] = self.cid
        post_data['imei'] = self.imei

        if ttid == '':
            post_data['ttid'] = self.ttid
        else:
            post_data['ttid'] = ttid

        if timestamps == '':
            post_data['t'] = self.t
        else:
            post_data['t'] = timestamps

        if sid != '':
            post_data['sid'] = sid
        else:
            sid = ''

        post_data['data'] = data
        post_data['appKey'] = self.app_key
        post_data['api'] = api
        post_data['v'] = version
        
        if sign != '':
            post_data['sign'] = sign
        else:
            sign_value = self.app_sign(post_data)
            post_data['sign'] = sign_value
        
        request = urllib2.Request(server_url)

        if sid != '':
            request.add_header('Cookie', val='JSESSIONID=' + sid)

        #request.add_header('User-Agent', val='金贝塔2.4 2.4.1 rv:5438 (iPhone; iPhone OS 9.2; zh-Hans_US)')
        if fileupload != '':
            post_data['module'] = module 
            fileupload = fileupload.split(',', 1)
            print post_data
            content_type, body = self.encode_multipart_formdata(post_data, fileupload)
            request.add_header('Accept-Encoding', 'gzip')
            request.add_header('Content-Type', content_type)
            request.add_data(body)
        else:
            request.add_header('Accept-Encoding', 'gzip')
            request.add_header('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8')
            print urllib.urlencode(post_data)
            request.add_data(urllib.urlencode(post_data))
            
        response = urllib2.urlopen(request, context=ssl.SSLContext(ssl.PROTOCOL_TLSv1))
        res = response.read()
        #print "response is :" + res
        self.result = json.loads(res)
        return self.result

    def code_should_be(self, code='1000000'):
        if self.result['code'] != code:
            raise AssertionError("Expected code is '%s' but was '%s'" %(code, self.result['code']))


if __name__ == '__main__':
    # datetime.now()
    # dt = datetime.now()
    # # 只有周一到周五才跑脚本
    # if dt.weekday() != 5 or dt.weekday() != 6:
    #     test_parse_json()

    #stocks_str = '''1,150,264169,156,219513,226803,8275,862,8281,8284,264209,106,410,226696,403,110,235796,75,8269,14489,711,8390,716,902,948,549,380,2273,383,488,130158,470,79,471,117175,142736,7659,91,25672,69322,49782,84775,49625,90787,163,48865,387,487,428,592,147,258,7401,478,85115,3688,1633,104,87,123,76,77,519,944,451,24042,49549,14516,3405,21834,80,291,2745,2620,364,138,102,73,81,68'''

    beta_service = Beta_Service()
    # beta_list = json.loads(stocks_str,encoding='utf-8')
    # beta_list = beta_list['data']['list']
    #beta_list = stocks_str.split(',')
    # for basketid in reversed(beta_list):
    #     beta_service.post_data(api='api.system.user.follow',
    #                        data='{"followid" : "'+ basketid + '","uid" : "116","type" : "0"}',
    #                        timestamps='1461292571',
    #                        sid='3ACD3D45A972DF7E57F481CF6B956EBB-n1',
    #                        version='1.0')

    
    beta_service.post_data(url='http://10.31.74.101:8081/cn-jsfund-server-mobile/common/files',
                        api='api.system.common.fileupload',
                       data='',
                       version='1.0',
                       sid='9226704873AC532DBD8E41898F837708',
                       fileupload='D:\\screenshot.png',
                       module='1')
    beta_service.code_should_be('1000000')
    # beta_service.post_data(api='api.system.stock.favorite',
    #                    data='{"code" :"' + stock_code + '","uid" : "116","type" : "0"}',
    #                    timestamps='1461292571',
    #                    sid='3ACD3D45A972DF7E57F481CF6B956EBB-n1',
    #                    version='1.0')