#!/usr/bin/env python
# -*- coding:utf-8 -*-

#from  openpyxl.workbook  import  Workbook  
#from  openpyxl.writer.excel  import  ExcelWriter
from  openpyxl import *
from openpyxl.styles import *
from openpyxl.reader.excel import load_workbook
from random import random
import time
import datetime
import sys
from string import split

reload(sys)
sys.setdefaultencoding( "gb2312" )


class excel():
    @staticmethod
    def getCellData(file,sheetName,row,col):
        """ get the specific cell data
        """
        excelrow=int(row)
        excelcol=int(col)

        wb = load_workbook(filename = file,data_only=True,guess_types=True)
        #ws = wb.worksheets[sheetNum]
        ws=wb.get_sheet_by_name(sheetName)
        excelText = ws.cell(row = excelrow,column =excelcol).value
        excelText = str(excelText)

        wb = None
        return excelText
        
    def writeCellData(self,file,sheetName,row,col,value):
        """ write the data to the specific cell
        """
        excelrow=int(row)
        excelcol=int(col)

        wb = load_workbook(filename = file)
        #ws = wb.worksheets[sheetNum]
        ws=wb.get_sheet_by_name(sheetName)
        ws.cell(row = excelrow,column =excelcol).value=value
        wb.save(filename = file)
        wb = None
    
    @staticmethod
    def excelRowCount(file,sheetName):
        """ get the maximum row number
        """
        #sheetNum=int(sheetNum)-1
        wb = load_workbook(filename = file)
        #ws = wb.worksheets[sheetNum]
        ws=wb.get_sheet_by_name(sheetName)
        rowcount=ws.get_highest_row()
        wb = None
		
        return rowcount

    @staticmethod
    def excelColCount(file,sheetName):
        """ get the maximum column number
        """
        #sheetNum=int(sheetNum)-1
        wb = load_workbook(filename = file)
        #ws = wb.worksheets[sheetNum]
        ws=wb.get_sheet_by_name(sheetName)
        colcount=len(ws.columns)
        wb = None
        return colcount
    
    @staticmethod
    def excelSheetCount(file):
        """ get the maximum sheet number
        """
        
        wb = load_workbook(filename = file)
        #ws=wb.get_sheet_by_name(sheetNum)
        sheetcount=len(wb.sheetnames)
        wb = None
		
        return sheetcount
        
    def searchExcelRow(self,file,sheetName,text):
        """ search the excel row by the specific text
        """
        #sheetNum=int(sheetNum)-1
        wb = load_workbook(filename = file,data_only=True,guess_types=True)
        #ws = wb.worksheets[sheetNum]
        ws=wb.get_sheet_by_name(sheetName)
        excelrow=ws.get_highest_row()
        excelcol=len(ws.columns)
        for i in range(1,excelrow+1):
            for j in range(1,excelcol+1):
                actureValue=ws.cell(row = i,column =j).value
                if actureValue == text:
                    print "found"
                    break
                else:
                    continue
                    #print actureValue
            if actureValue == text:
                print i
                break
        if actureValue == text:
            result=i
        else:
            result=0
        wb = None
		
        return result
        
    def searchExcelCol(self,file,sheetName,rowNo,text):
        """ search the excel column by the specific text
        """
        #sheetNum=int(sheetNum)-1
        rowNo = int(rowNo)
        wb = load_workbook(filename = file,data_only=True,guess_types=True)
        #ws = wb.worksheets[sheetNum]
        ws=wb.get_sheet_by_name(sheetName)
        excelrow=ws.get_highest_row()
        excelcol=len(ws.columns)

        result=0
        if rowNo > excelrow:
            raise ValueError,'invalid row number'
        else:
            for j in range(1,excelcol+1):
                actureValue=ws.cell(row = rowNo,column =j).value
                if actureValue == text:
                    print 'found'
                    result = j

        return result
        wb = None

 
        
    def changeCellFontColor(self,file,sheetName,row,col,RGBvalue):
        """ change cell color
        """
        excelrow=int(row)
        excelcol=int(col)
        #sheetNum=int(sheetNum)-1
        wb = load_workbook(filename = file)
        #ws = wb.worksheets[sheetNum]
        ws=wb.get_sheet_by_name(sheetName)
        s = ws.cell(row = excelrow,column =excelcol)
        #ws=wb.get_sheet_by_name(sheetNum)
        s.style=Style(font=Font(color=RGBvalue))
        wb.save(filename = file)
        wb = None



if __name__ == '__main__':
    s=excel()
    excelPath = u'D:/GoldenBeta/Api Test2/data/api.template.xlsx'
    url = s.getCellData(excelPath,'sms',1,3)
    print url
    api = s.excelColCount(excelPath,'sms')
    print api

    colnum = s.searchExcelCol(excelPath,'sms',1,'ActualRst');
    print colnum

        
