DELIMITER $$
drop function if exists `goldbeta2`.`beta_favorite`$$
create function `goldbeta2`.`beta_favorite`(betaid int, fans_num int) returns bool
begin
declare id int;
declare id_num int;
set id = 66;
set id_num = fans_num + id;
DELETE FROM goldbeta2.business_acc_gb_beta_favorite WHERE beta_id = betaid;
while id < id_num do
	INSERT INTO goldbeta2.business_acc_gb_beta_favorite(create_time,beta_id,customer_id) VALUES('2016-04-13 15:57:03',betaid,id);
    set id = id + 1;
end while;
return true;
end $$
DELIMITER ;  

select goldbeta2.beta_favorite(1002130844, 16000);
select goldbeta2.beta_favorite(1001380353, 16000);
select goldbeta2.beta_favorite(1001159962, 16000);
select goldbeta2.beta_favorite(1001012352, 16000);
select goldbeta2.beta_favorite(3658, 16000);