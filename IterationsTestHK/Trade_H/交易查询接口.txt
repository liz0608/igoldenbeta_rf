*** Settings ***
Resource          ../../Common/tradeH_common_resource.txt

*** Test Cases ***
查询资金，成功
    [Tags]    smoking    trade
    ${retCode}    tradeH_request_Get    queryService.queryFund    {}    ${sessionId}    returnlist=message,data
    Should Be Equal    ${retCode[0]}    请求成功

查询持仓数据成功
    [Tags]    smoking    trade
    ${retCode}    tradeH_request_Get    queryService.queryStock    {"stkcode": "00700.HK"}    ${sessionId}    returnlist=message,data
    Should Be Equal    ${retCode[0]}    请求成功

查询订单成功
    [Tags]    smoking    trade
    ${retCode}    tradeH_request_Get    queryService.queryOrders    {}    ${sessionId}    returnlist=message,data
    Should Be Equal    ${retCode[0]}    请求成功

搜索股票列表
    [Tags]    smoking    trade
    ${retCode}    tradeH_request_Get    queryService.queryMatch    {'pattern':'007'}    ${sessionId}    returnlist=message,data
    Should Be Equal    ${retCode[0]}    请求成功

股票精确查询
    [Tags]    smoking    trade
    ${retCode}    tradeH_request_Get    queryService.queryMatchExact    {"stkcode":"00700"}    ${sessionId}    returnlist=message
    Should Be Equal    ${retCode[0]}    请求成功

查询一次股票行情，行情礼包次数减少一次
    [Tags]
    ${ret}    tradeH_request_Get    queryService.queryQuotationCredit    {}    ${sessionId}    returnlist=data.creditRemain
    ${retCode}    tradeH_request_Get    queryService.queryMarket    {'code':'00700'}    ${sessionId}    returnlist=message,data
    Should Be Equal    ${retCode[0]}    请求成功
    ${ret1}    tradeH_request_Get    queryService.queryQuotationCredit    {}    ${sessionId}    returnlist=data.creditRemain
    should be true    ${ret[0]}-${ret1[0]} ==1

查询股票详情成功
    [Tags]
    ${retCode}    tradeH_request_Get    queryService.queryMarket    {'code':'00700'}    ${sessionId}    returnlist=message,data
    Should Be Equal    ${retCode[0]}    请求成功

查询有风险的股票
    [Tags]    smoking    trade
    ${retCode}    tradeH_request_Get    tradeService.chechBilker    {"secuEncode":"00260.HK"}    ${sessionId}    returnlist=message,data
    Should Be Equal    ${retCode[0]}    请求成功
    Should Be True    len(${retCode[1]}) > 4

查询无风险的股票
    [Tags]    smoking    trade
    ${retCode}    tradeH_request_Get    tradeService.chechBilker    {"secuEncode":"00700.HK"}    ${sessionId}    returnlist=message,data
    Should Be Equal    ${retCode[0]}    请求成功
    Should Be True    len(${retCode[1]}) <3

查询行情礼包成功
    [Tags]    smoking    trade
    ${retCode}    tradeH_request_Get    queryService.queryQuotationCredit    {}    ${sessionId}    returnlist=message,data.creditRemain
    Should Be Equal    ${retCode[0]}    请求成功
    should be true    ${retCode[1]}>=0

查询可用资金
    ${retCode}    tradeH_request_Get    queryService.queryAvailableFund    {}    ${sessionId}    returnlist=message,data
    Should Be Equal    ${retCode[0]}    请求成功
