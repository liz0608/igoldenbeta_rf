*** Settings ***
Suite Setup
Suite Teardown
Resource          ../../Common/all_common_resourse_HK.txt

*** Test Cases ***
获取密码公钥
    [Tags]    smoking    trade
    ${return}    mobile_postHK    api.system.account.getpublickey    {"uid":"${uid}"}    ${sid}    code,data.publicKeyString
    Should Be Equal As Strings    ${return[0]}    1000000
    Should Be True    len($return[1])>100

交易api登录
    [Tags]    smoking    trade
    ${verifycode}    Evaluate    ''.join([random.choice(string.digits) for i in range(4)])    random,string
    ${return}    mobile_postHK    api.system.account.login    {"trdpwd":"${trdpwd}","uid":"${uid}","verifyCode":"${verifycode}"}    ${sid}    code,data.sessionId
    Should Be Equal As Strings    ${return[0]}    1000000
    Set Suite Variable    ${sessionId}    ${return[1]}

交易api查询资金
    [Documentation]    api.system.account.queryfund
    [Tags]    smoking    trade
    ${ret}    mobile_postHK    api.system.account.queryfund    {"uid":"${uid}","sessionId":"${sessionId}"}    ${sid}    code
    Should Be Equal As Strings    ${ret[0]}    1000000

交易api查询持仓
    [Documentation]    api.system.account.querystock
    [Tags]    smoking    trade
    ${ret}    mobile_postHK    api.system.account.querystock    {"uid":"${uid}","sessionId":"${sessionId}"}    ${sid}    code
    Should Be Equal As Strings    ${ret[0]}    1000000

交易api查询当日交易
    [Documentation]    api.system.account.queryorders
    ...
    ...    istoday:1
    [Tags]    smoking    trade
    ${ret}    mobile_postHK    api.system.account.queryorders    {"uid":"${uid}","sessionId":"${sessionId}"}    ${sid}    code
    Should Be Equal As Strings    ${ret[0]}    1000000
